package com.example.herlin.projectplbtw.admin.userinterface;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.addadmin.userinterface.AddAdminActivity;
import com.example.herlin.projectplbtw.admin.adapter.RecyclerAdminAdapter;
import com.example.herlin.projectplbtw.admin.api.ApiAdmin;
import com.example.herlin.projectplbtw.admin.model.AdminListResponse;
import com.example.herlin.projectplbtw.admin.model.AdminModel;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AdminListActivity extends Fragment {
    /*
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_admin);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }
    */
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";

    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";


    View rootview;

    public static final String API_KEY = "asdfghjkl";
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    //   @Bind(R.id.contentChallengeList)
    //   LinearLayout contentChallengeList;

    @Bind(R.id.listAdmin)
    RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<AdminModel> adminModelList;
    private ApiAdmin apiAdmin = null;
    private RecyclerAdminAdapter adapter;

    @Bind(R.id.fab)
    FloatingActionButton fab;

    @OnClick(R.id.fab)
    void addAdmin(View view) {
        Intent i = new Intent(getContext().getApplicationContext(), AddAdminActivity.class);
        startActivity(i);

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        rootview = inflater.inflate(R.layout.activity_admin_list, container, false);
        //       getActivity().setTitle("Challenge");
        ButterKnife.bind(this, rootview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity().getApplicationContext()));
        progressBar.setVisibility(View.VISIBLE);

        sharedPreferences = getActivity().getSharedPreferences(sharedPreferenceName, 0);
        String role = sharedPreferences.getString(roleMarker, "0");
        if (role.equalsIgnoreCase("2")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.INVISIBLE);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter == null) {
                    progressBar.setVisibility(View.VISIBLE);
                }
                getAdminList(false);
            }
        });
        apiAdmin = ServiceGenerator
                .createService(ApiAdmin.class);
        getAdminList(true);
        return rootview;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        getAdminList(true);
        Snackbar.make(coordinatorLayout, "Sedang memperbaharui data admin", Snackbar.LENGTH_LONG).show();
    }

    private void getAdminList(boolean pBar) {
        if (pBar)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
        Call<AdminListResponse> call = null;
        call = apiAdmin.getAdminList("1", "1000", API_KEY);

//        call = apiReward.getHerritageList();
        call.enqueue(new Callback<AdminListResponse>() {
            @Override
            public void onResponse(Response<AdminListResponse> response,
                                   Retrofit retrofit) {
            //    Toast.makeText(getActivity().getApplicationContext(), response.code() + "", Toast.LENGTH_SHORT).show();
                if (2 == response.code() / 100 && response.isSuccess()) {
                    showAdminList(response);
                } else {
                    showErrorMessage(response);
                }
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                //        contentChallengeList.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan..",
                        Snackbar.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                //         contentChallengeList.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showErrorMessage(Response<AdminListResponse> response) {
        Snackbar.make(coordinatorLayout, "Gagal mengambil konten admin" + response.code(), Snackbar.LENGTH_LONG).show();
    }

    private void showAdminList
            (Response<AdminListResponse> response) {

        final AdminListResponse challengeResponse = response.body();
        adminModelList = challengeResponse.getListAdmin();
        if (adminModelList != null) {
            adapter = new RecyclerAdminAdapter(adminModelList, recyclerView);
            adapter.setCoordinatorLayout(coordinatorLayout);
            if (getActivity() != null) {
                adapter.setContext(getActivity().getApplicationContext());
                adapter.setInflater(getActivity().getLayoutInflater());
                adapter.setAdminActivity(this);
                recyclerView.setAdapter(adapter);
            }
        }
    }
}

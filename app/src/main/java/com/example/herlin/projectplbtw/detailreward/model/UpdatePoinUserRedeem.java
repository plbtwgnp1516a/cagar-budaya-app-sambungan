package com.example.herlin.projectplbtw.detailreward.model;

/**
 * Created by VAIO on 6/4/2016.
 */
public class UpdatePoinUserRedeem {
    private String api_key;
    private String poin;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getPoin() {
        return poin;
    }

    public void setPoin(String poin) {
        this.poin = poin;
    }
}

package com.example.herlin.projectplbtw.addchallenge.api;

import com.example.herlin.projectplbtw.addchallenge.model.AddChallengeModel;
import com.example.herlin.projectplbtw.addchallenge.model.AddChallengeResponse;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageModel;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by VAIO on 5/21/2016.
 */
public interface ApiAddChallenge {
    @POST("challenge")
    Call<AddChallengeResponse> postChallenge(@Body AddChallengeModel addChallengeModel);
}

package com.example.herlin.projectplbtw.detailreward.model;

/**
 * Created by VAIO on 6/3/2016.
 */
public class RedeemObjectResponse {
    private String id_redeem;
    private String redem_key;
    private String id_user;
    private String id_reward;

    public String getId_redeem() {
        return id_redeem;
    }

    public void setId_redeem(String id_redeem) {
        this.id_redeem = id_redeem;
    }

    public String getRedem_key() {
        return redem_key;
    }

    public void setRedem_key(String redem_key) {
        this.redem_key = redem_key;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_reward() {
        return id_reward;
    }

    public void setId_reward(String id_reward) {
        this.id_reward = id_reward;
    }
}

package com.example.herlin.projectplbtw.reward.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by VAIO on 5/16/2016.
 */
public class RewardResponse {

    @SerializedName("content")
    private ArrayList<RewardModel> listReward = new ArrayList<>();

    public ArrayList<RewardModel> getListReward() {
        return listReward;
    }

    public void setListReward(ArrayList<RewardModel> listReward) {
        this.listReward = listReward;
    }
}


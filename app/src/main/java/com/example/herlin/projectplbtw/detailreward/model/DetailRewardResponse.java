package com.example.herlin.projectplbtw.detailreward.model;

/**
 * Created by VAIO on 5/31/2016.
 */
public class DetailRewardResponse {
    private Boolean Success;
    private String Info;

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }
}

package com.example.herlin.projectplbtw.detailreward.userinterface;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.detailcagar.api.ApiDetailCagar;
import com.example.herlin.projectplbtw.detailreward.api.ApiDetailReward;
import com.example.herlin.projectplbtw.detailreward.model.DetailRewardResponse;
import com.example.herlin.projectplbtw.detailreward.model.RedeemModel;
import com.example.herlin.projectplbtw.detailreward.model.RedeemResponse;
import com.example.herlin.projectplbtw.detailreward.model.UpdatePoinUserRedeem;
import com.example.herlin.projectplbtw.editreward.userinterface.EditRewardActivity;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailRewardActivity extends AppCompatActivity {

    public static final String API_KEY = "asdfghjkl";

    private String id_reward;
    private String nama_reward;
    private String deskripsi;
    private String minimal_poin;

    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private ApiDetailReward apiDetailReward = null;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @Bind(R.id.textNamaReward)
    TextView textNamaReward;
    @Bind(R.id.textMinimalPoin)
    TextView textMinimalPoin;
    @Bind(R.id.textDeskripsi)
    TextView textDeskripsi;
    @Bind(R.id.btnDelete)
    Button btnDelete;
    @Bind(R.id.btnEdit)
    Button btnEdit;
    @Bind(R.id.btnRedeem)
    Button btnRedeem;

    @OnClick(R.id.btnDelete)
    void setOnClickButtonDelete(final View view) {
        final android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialog.setMessage(Html.fromHtml("<strong>Apakah anda yakin ingin menghapus reward ini?</strong>"));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
                final ProgressDialog dialog = new ProgressDialog(view.getContext());
                dialog.setMessage("Mohon tunggu...");
                dialog.show();

                Call<DetailRewardResponse> call = null;
                call = apiDetailReward.deleteReward(id_reward);
                call.enqueue(new Callback<DetailRewardResponse>() {
                    @Override
                    public void onResponse(Response<DetailRewardResponse> response, Retrofit retrofit) {
                        //      Toast.makeText(DetailRewardActivity.this, response.code()+"", Toast.LENGTH_SHORT).show();

                        if (2 == response.code() / 100) {
                            Toast.makeText(DetailRewardActivity.this, "Hadiah Berhasil Terhapus", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            dialog.dismiss();
                            Toast.makeText(DetailRewardActivity.this, "Gagal menghapus Hadiah", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dialog.dismiss();
                        Toast.makeText(DetailRewardActivity.this, "Terjadi kesalahan jaringan", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
            }
        });
        alertDialog.show();

    }

    @OnClick(R.id.btnEdit)
    void setOnClickBtnEdit(View view) {

        Intent i = new Intent(getApplicationContext(), EditRewardActivity.class);
        Bundle b = new Bundle();
        b.putString("nama_reward", nama_reward);
        b.putString("deskripsi", deskripsi);
        b.putString("minimal_poin", minimal_poin);
        b.putString("id_reward", id_reward);
        i.putExtras(b);

        startActivity(i);
        finish();

    }

    @OnClick(R.id.btnRedeem)
    void setOnClickBtnRedeem(View view) {
        updatePoinUser();
    }

    private void insertRedeem() {
        final ProgressDialog dialog = new ProgressDialog(DetailRewardActivity.this);
        dialog.setMessage("Mohon tunggu...");
        dialog.setCancelable(false);
        dialog.show();
        Call<RedeemResponse> call = null;

        RedeemModel redeemModel = new RedeemModel();
        redeemModel.setId_reward(id_reward);
        redeemModel.setId_user(sharedPreferences.getString(idUserMarker, ""));
        call = apiDetailReward.postRedeem(redeemModel);
        call.enqueue(new Callback<RedeemResponse>() {
            @Override
            public void onResponse(Response<RedeemResponse> response, Retrofit retrofit) {
                dialog.dismiss();
                Toast.makeText(DetailRewardActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();

                if (2 == response.code() / 100) {
                    Intent i = new Intent(getApplicationContext(), ShowRedeemKeyActivity.class);
                    Bundle b = new Bundle();
                    b.putString("redeem_key", response.body().getContent().get(0).getRedem_key());
                    i.putExtras(b);
                    startActivity(i);
                    finish();

                } else {
                    Toast.makeText(DetailRewardActivity.this, "Gagal melakukan redeem", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(DetailRewardActivity.this, "Terjadi kesalahan jaringan..", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void updatePoinUser() {

        if (Integer.parseInt(minimal_poin) < Integer.parseInt(sharedPreferences.getString(poinMarker, "0"))) {

            final ProgressDialog dialog = new ProgressDialog(DetailRewardActivity.this);
            dialog.setMessage("Mohon tunggu...");
            dialog.setCancelable(false);
            dialog.show();
            Call<DetailRewardResponse> call = null;

            UpdatePoinUserRedeem updatePoinUserRedeem = new UpdatePoinUserRedeem();
            updatePoinUserRedeem.setApi_key(API_KEY);
            updatePoinUserRedeem.setPoin(textMinimalPoin.getText().toString());

            call = apiDetailReward.updatePoinUserAfterRedeem(sharedPreferences.getString(idUserMarker, ""), updatePoinUserRedeem);

            call.enqueue(new Callback<DetailRewardResponse>() {
                @Override
                public void onResponse(Response<DetailRewardResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                    Toast.makeText(DetailRewardActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true) {
                            //                  moveToDetail();

                            insertRedeem();
                        } else {
                            Snackbar.make(coordinatorLayout, "Gagal mengubah data hadiah", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    } else {
                        Snackbar.make(coordinatorLayout, "Gagal mengubah data hadiah", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan..", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    //     textNamaCagar.setText(t.getMessage()+"");

                }
            });
        } else

        {
            Toast.makeText(DetailRewardActivity.this, "Poin anda tidak mencukupi untuk melakukan redeem!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_reward);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiDetailReward = ServiceGenerator.createService(ApiDetailReward.class);
        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        Bundle b = getIntent().getExtras();
        nama_reward = b.getString("nama_reward");
        deskripsi = b.getString("deskripsi");
        minimal_poin = b.getString("minimal_poin");
        id_reward = b.getString("id_reward");
        textNamaReward.setText(nama_reward);
        textDeskripsi.setText(deskripsi);
        textMinimalPoin.setText(minimal_poin);

        Picasso.with(getApplicationContext()).load(nama_reward)
                .placeholder(R.drawable.gift).into(imageThumbnail);


        if (sharedPreferences.getString(roleMarker, "").equalsIgnoreCase("1")) {
            btnDelete.setVisibility(View.INVISIBLE);
            btnEdit.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}

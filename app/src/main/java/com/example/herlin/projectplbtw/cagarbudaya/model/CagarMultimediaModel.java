package com.example.herlin.projectplbtw.cagarbudaya.model;

/**
 * Created by VAIO on 6/6/2016.
 */
public class CagarMultimediaModel {
    private String id_multimedia;
    private String nama_multimedia;
    private String url;
    private String tipe;
    private String nama;
    private String email;
    private String telepon;
    private String poin;

    public String getId_multimedia() {
        return id_multimedia;
    }

    public void setId_multimedia(String id_multimedia) {
        this.id_multimedia = id_multimedia;
    }

    public String getNama_multimedia() {
        return nama_multimedia;
    }

    public void setNama_multimedia(String nama_multimedia) {
        this.nama_multimedia = nama_multimedia;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getPoin() {
        return poin;
    }

    public void setPoin(String poin) {
        this.poin = poin;
    }
}

package com.example.herlin.projectplbtw.detailcagar.userinterface;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.ShareTwitActivity;
import com.example.herlin.projectplbtw.WebViewActivity;
import com.example.herlin.projectplbtw.addherritage.api.ApiAddHerritage;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageResponse;
import com.example.herlin.projectplbtw.addherritage.model.AddPointModel;
import com.example.herlin.projectplbtw.detailcagar.api.ApiDetailCagar;
import com.example.herlin.projectplbtw.detailcagar.model.AcceptBody;
import com.example.herlin.projectplbtw.detailcagar.model.DetailCagarResponse;
import com.example.herlin.projectplbtw.detailreward.userinterface.DetailRewardActivity;
import com.example.herlin.projectplbtw.editherritage.userinterface.EditHerritageActivity;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class DetailCagarActivity extends AppCompatActivity {

    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";

    private static SharedPreferences mSharedPreferences;

    private String nama_jenis;
    private String nama_cagar;
    private String deskripsi;
    private String tahun;
    private String id_cagar;
    private String id_user;
    private String alamat;
    private String longitude;
    private String latitude;
    private String status;
    private String nama;
    private String url;
    private String eksekusi;

    public static final String API_KEY="asdfghjkl";
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private ApiDetailCagar apiDetailCagar=null;
    private ApiAddHerritage apiAddHerritage = null;


    @Bind(R.id.textPengirim)
    TextView textPengirim;
    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @Bind(R.id.textNamaCagar)
    TextView textNamaCagar;
    @Bind(R.id.textTahun)
    TextView textTahun;
    @Bind(R.id.textJenis)
    TextView textJenis;
    @Bind(R.id.textAlamat)
    TextView textAlamat;
    @Bind(R.id.textDeskripsi)
    TextView textDeskripsi;
    @Bind(R.id.btnDelete)
    Button btnDelete;
    @Bind(R.id.btnEdit)
    Button btnEdit;
    @Bind(R.id.btnAccept)
    Button btnAccept;
    @Bind(R.id.btnShare)
    Button btnShare;
    @Bind(R.id.btnTambahResource)
    Button btnTambahResource;
    @Bind(R.id.btnGallery)
    Button btnGallery;

    @OnClick(R.id.btnGallery)
    void setOnClickBtnGallery(View view)
    {
        Intent i=new Intent(getApplicationContext(),GalleryActivity.class);
        Bundle b=new Bundle();
        b.putString("id_user",id_user);
        b.putString("id_cagar",id_cagar);
        i.putExtras(b);
        startActivity(i);
        finish();

    }
    @OnClick(R.id.btnTambahResource)
    void setOnClickBtnTambahResource(View view)
    {
        Intent i=new Intent(getApplicationContext(),AddImgResourceActivity.class);
        Bundle b=new Bundle();
        b.putString("id_user",id_user);
        b.putString("id_cagar",id_cagar);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.btnShare)
    void loginToTwitter(View view)
    {
        loginToTwitter();
    }

    @OnClick(R.id.btnAccept)
    void setOnClickBtnAcc(View view)
    {
        final ProgressDialog dialog = new ProgressDialog(DetailCagarActivity.this);
        dialog.setMessage("Updating user point");
        dialog.setCancelable(false);
        dialog.show();
        Call<DetailCagarResponse> call = null;
        AcceptBody acceptBody = new AcceptBody();
        acceptBody.setApi_key(API_KEY);
        acceptBody.setStatus("accepted");
        call = apiDetailCagar.acceptCagar(id_cagar,acceptBody);

        call.enqueue(new Callback<DetailCagarResponse>() {
            @Override
            public void onResponse(Response<DetailCagarResponse> response, Retrofit retrofit) {
                dialog.dismiss();
                Toast.makeText(DetailCagarActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                if (2 == response.code() / 100) {
                    // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                    if (response.body().getSuccess() == true)
                  //      finish();
                        addUserPoint();
                    else
                        Toast.makeText(DetailCagarActivity.this, "Gagar memverifikasi cagar budaya", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(DetailCagarActivity.this, "Gagal memverifikasi cagar budaya", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(DetailCagarActivity.this, t.getMessage()+"Terjadi kesalahan jaringan.", Toast.LENGTH_SHORT).show();

                //     textNamaCagar.setText(t.getMessage()+"");

            }
        });
    }
    @OnClick(R.id.btnEdit)
    void setOnClickBtnEdit(View view)
    {
        Bundle b = new Bundle();
        b.putString("nama_cagar",nama_cagar);
        b.putString("deskripsi",deskripsi);
        b.putString("tahun",tahun);
        b.putString("id_cagar",id_cagar);
        b.putString("id_user",id_user);
        b.putString("alamat",alamat);
        b.putString("longitude",longitude);
        b.putString("latitude",latitude);
        b.putString("status",status);
        b.putString("nama_jenis",nama_jenis);
        b.putString("nama",nama);
        Intent i = new Intent(getApplicationContext(), EditHerritageActivity.class);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.btnDelete)
    void setOnClickButtonDelete(final View view)
    {
        final android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialog.setMessage(Html.fromHtml("<strong>Apakah anda yakin ingin menghapus cagar budaya ini?</strong>"));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
                final ProgressDialog dialog = new ProgressDialog(view.getContext());
                dialog.setMessage("Please wait...");
                dialog.show();

                Call<DetailCagarResponse> call = null;
                call = apiDetailCagar.deleteCagar(id_cagar,API_KEY);
                call.enqueue(new Callback<DetailCagarResponse>() {
                    @Override
                    public void onResponse(Response<DetailCagarResponse> response, Retrofit retrofit) {
                        Toast.makeText(DetailCagarActivity.this, response.code()+"", Toast.LENGTH_SHORT).show();
                        if (2 == response.code() / 100) {
                            Toast.makeText(DetailCagarActivity.this, "Cagar Budaya Berhasil Terhapus", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            dialog.dismiss();
                            Toast.makeText(DetailCagarActivity.this, "Gagal menghapus cagar budaya ", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dialog.dismiss();
                        Toast.makeText(DetailCagarActivity.this, "Terjadi kesalahan jaringan..", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
            }
        });
        alertDialog.show();

    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        	/* initializing twitter parameters from string.xml */
        initTwitterConfigs();
        /* Enabling strict mode */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_detail_cagar);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiDetailCagar = ServiceGenerator.createService(ApiDetailCagar.class);
        apiAddHerritage = ServiceGenerator.createService(ApiAddHerritage.class);

        mSharedPreferences = getSharedPreferences(PREF_NAME, 0);

        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);

        Bundle b = getIntent().getExtras();
        nama_cagar=b.getString("nama_cagar");
        deskripsi=b.getString("deskripsi");
        tahun=b.getString("tahun");
        id_cagar=b.getString("id_cagar");
        id_user=b.getString("id_user");
        alamat=b.getString("alamat");
        longitude=b.getString("longitude");
        latitude=b.getString("latitude");
        status=b.getString("status");
        nama_jenis=b.getString("nama_jenis");
        nama=b.getString("nama");
        eksekusi=b.getString("eksekusi");
        url=b.getString("url");
        textNamaCagar.setText(nama_cagar);
        textDeskripsi.setText(deskripsi);
        textTahun.setText("Tahun didirikan: "+tahun);
        textAlamat.setText(alamat);
        textJenis.setText("Jenis: "+nama_jenis);
//        Picasso.with(getApplicationContext()).load(url)
  //              .placeholder(R.drawable.cabud4).into(imageThumbnail);

        Picasso.with(getApplicationContext()).load(url)
                .placeholder(R.drawable.cabud4).into(imageThumbnail);

     //   Toast.makeText(DetailCagarActivity.this, url, Toast.LENGTH_SHORT).show();
        textPengirim.setText("Kiriman dari : "+nama);
        if(sharedPreferences.getString(roleMarker,"").equalsIgnoreCase("1")) {
            btnDelete.setVisibility(View.GONE);
            btnEdit.setVisibility(View.GONE);
            btnAccept.setVisibility(View.GONE);
        }
        else
        {
            if(eksekusi.equalsIgnoreCase("heritage")) {
                btnAccept.setVisibility(View.GONE);
            }
            else
                btnShare.setVisibility(View.GONE);

        }
        btnGallery.setVisibility(View.GONE);
        btnTambahResource.setVisibility(View.GONE);
    }

    private static final String PREF_NAME = "sample_twitter_pref";
    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;

    private static Twitter twitter;
    private static RequestToken requestToken;
    /* Any number for uniquely distinguish your request */
    public static final int WEBVIEW_REQUEST_CODE = 100;

    /* Reading twitter essential configuration parameters from strings.xml */
    private void initTwitterConfigs() {
        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        callbackUrl = getString(R.string.twitter_callback);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);
    }

    private void loginToTwitter() {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                requestToken = twitter.getOAuthRequestToken(callbackUrl);

                /**
                 *  Loading twitter login page on webview for authorization
                 *  Once authorized, results are received at onActivityResult
                 *  */
                final Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                startActivityForResult(intent, WEBVIEW_REQUEST_CODE);

            } catch (TwitterException e) {
                e.printStackTrace();
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            String verifier = data.getExtras().getString(oAuthVerifier);
            try {
                AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

                long userID = accessToken.getUserId();
                final User user = twitter.showUser(userID);
                String username = user.getName();
                String description=user.getDescription();
                String profileImage=user.getOriginalProfileImageURL();
                String profileBanner=user.getProfileBannerMobileURL();
                String screenName=user.getScreenName();
                String tweetCount=user.getStatusesCount()+"";
                String followingCount=user.getFriendsCount()+"";
                String followerCount=user.getFollowersCount()+"";
                String likeCount=user.getFavouritesCount()+"";
                saveTwitterInfo(accessToken);


                Intent i=new Intent(getApplicationContext(), ShareTwitActivity.class);
                Bundle b=new Bundle();
                b.putString("nama_cagar",nama_cagar);
                b.putString("nama_jenis",nama_jenis);
                b.putString("alamat",alamat);
                i.putExtras(b);
                startActivity(i);
                finish();


            } catch (Exception e) {
                Log.e("Twitter Login Failed", e.getMessage());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void saveTwitterInfo(AccessToken accessToken) {

        long userID = accessToken.getUserId();

        User user;
        try {
            user = twitter.showUser(userID);

            String username = user.getName();
            String status = user.getStatus()+"";
            String description = user.getDescription();
            String profileImage= user.getOriginalProfileImageURL();
            String banner=user.getProfileBannerURL();//.getProfileBannerMobileURL();
            String screenName= user.getScreenName();

            String tweetCount=user.getStatusesCount()+"";
            String followingCount=user.getFriendsCount()+"";
            String followerCount=user.getFollowersCount()+"";
            String likeCount=user.getFavouritesCount()+"";

			/* Storing oAuth tokens to shared preferences */
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
            e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
            e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);

            e.commit();

        } catch (TwitterException e1) {
            e1.printStackTrace();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent();
            setResult(1, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void addUserPoint()
    {
        final ProgressDialog dialog = new ProgressDialog(DetailCagarActivity.this);
        dialog.setMessage("Updating user point");
        dialog.setCancelable(false);
        dialog.show();
        Call<AddHerritageResponse> call = null;
        AddPointModel addPointModel = new AddPointModel();
        addPointModel.setApi_key(API_KEY);
        addPointModel.setPoin("200");
        call = apiAddHerritage.addPoint(id_user,addPointModel);

        call.enqueue(new Callback<AddHerritageResponse>() {
            @Override
            public void onResponse(Response<AddHerritageResponse> response, Retrofit retrofit) {
                dialog.dismiss();
           //     Toast.makeText(DetailCagarActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                if (2 == response.code() / 100) {
                    // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                    if (response.body().getSuccess() == true)
                  //      moveToMenu();
                        finish();
                    else
                        Snackbar.make(coordinatorLayout, "Gagal menambahkan poin pengguna", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                } else {
                    Snackbar.make(coordinatorLayout, "Gagal menambahkan poin pengguna", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan..", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                //     textNamaCagar.setText(t.getMessage()+"");

            }
        });
    }

}

package com.example.herlin.projectplbtw.addherritage.api;

import com.example.herlin.projectplbtw.addherritage.model.AddHerritageModel;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageResponse;
import com.example.herlin.projectplbtw.addherritage.model.AddPointModel;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by VAIO on 5/21/2016.
 */
public interface ApiAddHerritage {
    @POST("cagarbudaya")
    Call<AddHerritageResponse> postHerritage(@Body AddHerritageModel addHerritageModel);




    @PUT("user/poin/{id_user}")
    Call<AddHerritageResponse> addPoint(@Path("id_user") String id_user,
                                      @Body AddPointModel addPointModel);
}

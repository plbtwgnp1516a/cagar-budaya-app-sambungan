package com.example.herlin.projectplbtw.detailcagar.userinterface;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.detailcagar.api.ApiDetailCagar;
import com.example.herlin.projectplbtw.detailcagar.model.DetailCagarResponse;
import com.example.herlin.projectplbtw.detailcagar.model.UploadImageBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AddImgResourceActivity extends AppCompatActivity {
    private String thumbnailCode;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddImgResourceActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        //  alamat=destination.getPath();
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        roundedImage = new RoundImage(thumbnail);
//        ivImage.setImageDrawable(roundedImage);
        imageThumbnail.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);
        //alamat = selectedImagePath;
        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        //     roundedImage = new RoundImage(bm);
        //     ivImage.setImageDrawable(roundedImage);
        imageThumbnail.setImageBitmap(bm);
    }


    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private ApiDetailCagar apiDetailCagar=null;
    public static final String API_KEY="asdfghjkl";
    private String id_cagar;
    @Bind(R.id.btnSelectImage)
    Button btnSelectImage;
    @Bind(R.id.btnSendImage)
    Button btnSendImage;
    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @OnClick(R.id.btnSendImage)
    void setOnClickBtnSendImage(View view)
    {
        final ProgressDialog dialog = new ProgressDialog(AddImgResourceActivity.this);
        dialog.setMessage("Mohon tunggu...");
        dialog.setCancelable(false);
        dialog.show();
        Call<DetailCagarResponse> call = null;
        UploadImageBody uploadImageBody = new UploadImageBody();
        uploadImageBody.setApi_key(API_KEY);
        uploadImageBody.setNama_multimedia("Gambar 1");
        uploadImageBody.setId_user(sharedPreferences.getString(idUserMarker,""));
        uploadImageBody.setId_cagar(id_cagar);
        uploadImageBody.setTipe("Gambar");
        Bitmap bitmap = ((BitmapDrawable) imageThumbnail.getDrawable()).getBitmap();
        thumbnailCode = getEncoded64ImageStringFromBitmap(bitmap);
        uploadImageBody.setImage_base64(thumbnailCode);
      /*
        uploadImageBody.setApi_key("asdfghjkl");
        uploadImageBody.setNama_multimedia("gambar1");
        uploadImageBody.setId_user("1");
        uploadImageBody.setId_cagar("1");
        uploadImageBody.setTipe("Gambar");
        uploadImageBody.setImage_base64("/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFRUXFxkYFxgXGRgYGhoYGhgYFxoXFx0YHighGB4lHRcYITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGhAQGy0lICUtLTUtLS0vLS8tLS8tLy8tLS0tLS0tLS0vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQADAQIGBwj/xABFEAABAwIDBQYDBAgEBQUBAAABAgMRACEEEjEFIkFRYQYTMnGBkaGxwRRCUvAHFSNicoLR4TOSovFDU5OzwhaDstLiRP/EABsBAAIDAQEBAAAAAAAAAAAAAAIDAAEEBQYH/8QAOREAAgECBQEFBgQEBwEAAAAAAAECAxEEEhMhMUEFIlFhcRQVgZGx8CMyQsFSodHhJDNDRWKy8Qb/2gAMAwEAAhEDEQA/AOGZTetsThc2gmhW8TFqPYdka17GcJR3OIvMUvYIitG8DI0NOFZibXo1nCSJFR1nFbhpt8HLLwZHOsYQqSbGukxbBTwpEsQuQLU+nPURed8MIU7xmDVCcQqdKIU1m01rODYUFhJ486qySFq1iHZZUcxVrpQ2Iwqk7us10KsMfCRbpwpbisEtKpmR+daXTnd8l5mhHicKtJgp9qGyxTrFKVxoRxAI61rgth0KrtuDpUTqai2xrrW/c1uhPmaPKFmS4BAKxmNXLbiq8tXkGJ3MIVYg8aJwWQDeBmh8tXIai5NRwBnZoJcxCEgpSkedL3DNbr1MaVXVaZIRSNMtTJVkVIqZA7leWplqyKkVMhdyvLWIq2KkVMhLlYFQpqwCpFXYq5XlqZatipFVlJmKstTLVsVkJqspMxWE1e0msJRWc4vwiL/T88vWs2LxkcHTzyV2+CRi5uyLp6E9RUoBWNVwKQPIVK8779x3ivkv6D/Z4HVMbPB60Zh8ApJ4RR2DZijkiuvUxDvY5sYArTQ5Ve2gCrCKxNZ2nIO9ip9AUKUu7JBNrU5NVketOpOUeBcrMUJwmW35NWLw+hBEg8aKffE3BqvvE2N602m97Ccy4LA6qNKX4hpckk2NMFODQD3rKATwmhinHexbdznFYf8AM0O8yAaf7Sa5J9RShSRNxauhS7yuVGbTNcHlmDpzrONwIF0G1VLABtWoUedP0ne4e97plLonz8zVGSjCKrKaNQGxkDpRWSNTV2SrmsESJqOJbqJbsX5axlponCJGptxrV5lOqQY51ViayuLctTLRPd1nJUyh5wcNVMlFA1qoTVZQc7BslTLROSshHOplLzg/cnWsFuiSmsBE1WUmcGyVkN0V3cET8K3JGgsOVTKVqAam4qyKJyzpYDjQeNxGSUiCefKsGK7Qw+GupvvLouQ4KU+DRbljHqenTrVP2YlGcWHkZJ6WiP7VQp5XAxPGb8vz60YlKISc9kiTBm5gZQemvrXjcdjKlepnk/T0N8IqEStvAkgEAmeMgfSpRaHRG63bhuz/AOVSufmkTU8ju8OQBV6FilTT5FXtHN0r3c8HJNuRw1iF0G6Eg1hbHKqsM0TxosNkVjcMr2ZoUsy4KAwa0cwxpkhNRQBNSNSSZHBMRlk/eBqosQIi1P1YcnhNU/ZsxuCK1QroRKmInAAIgn6UL9qUi2tN9ooy2At+daSvovXSw8IzV2tjNKVpWK8Ri1GhMszNElBrQt1ujTjFWRFIDLVa91RpaqdyaZsM1QLuqx3VGd3U7ur2L1AdpsW+NWF6tyite7ocqZWZPkpUBqZJrGvM9OFX93W7bJJgVHFF50CKZ6VoW66TB7DdVqLc9B8daoxuySgwYJ6XpKq028tyatuRDkrGSmRwsa2rRTYHKm2QSqgATUyUWpFa93V5As4LkqZaJ7usd3UyF5wbLWwRV/dVYhqooIp1BXtFowDNp0sPWSdaXOi9wb6cTTjbSIQLxz8qoawjIEySI4lJ4nSLcuPE14H/AOhcY4tteC8zpYaf4aYA7h9wKUMtrETvEcTw041UDGUlIgiw5jSSR1p+lBbbhKAUSJuFqAIIIj0OsaUHtfBypJQkqlIFhEHWDNtK4Mal3ZmiM7g6McuL5fY++tSmaNlqIBDSyOYKYPlapQ54BZf+J220thOMEZ4IIkFEkeRtY0KjD3rthjQJAJVPBSUkH40DtDDpXBQ0lB45Zg+nD0r3sMe3tM8/LC9UJmCedMGiTWUbLVEkjyitEoULVnqyhNuzRopqUVugnuSa2Q0a1ZUQbzR6Eg61llKw5K5RcCh3n/IUwKU86GxGDzcaOlKF+8DNStsJcU0FKn40K7ggeFMnNnrHUVUsRXQ1pRSySuY9NN95WFKtnmh38IUmnyTVTjc8KfQxdST7yFVaMYK8WIe6qd3TZxkkxFqwNnL/AA1t111Mt5X2FJarHdU6XspQE62oRTEVca8ZcMkpSjyLizWO5ph3VQNGj1CtUowmzFOKgA8/TnXRYPY7TcZiM0cT9KUNqUmY41oVK5mkVc9TbNZBRrJDTH49KYCFSRNoJA94FInsU4bZoHIWHwq0tmtS1RUqcYE1bvcDObnVZao/uqx3NaFMtVRf3dY7uj+6qd1RagWqAd1UDVHd1We6qOrYvVAQzWMRuJn6xFrfG1NAxSLtLjEhtORaSongZlMEHS2vOuf2hi9OhJp72G4a9WokKdp7QBlIukgC/wAdaYYTZy32VuSAYzDNbdBhR05kamPhPNYfMpUDU2ga+ldY1ji1hlKsvPlaQkeEJSDJE3AMq6yUnhXzfESnJ3veTfLPQSjlSURmU5mMzaEQAhOknMN4LMapkiROqYPEVyuOeWkn8JBvINtLHjp5/M9nkQlnu1qEhBaQYB3gT+0QQCpPiFo0zRArmO2GFJPfJVmSpRubG8CAOQKT13hreseHksziBSsnZitOJR+M/wCWfrUpYVVmt+mjVue3Jdvyo1jFAXpYqQbiqdoY4NNleQriLDraTyFd2rDKrs58JpnUIxqCKGTiG1jMhQUOYIIt1FedvbbcUtSZTYBWQapuBcgaCbn6VUMU+mN4QtUcQOHh1nzHWuRLtBRl3Vt8jS6btuej94DarVISZShQKkxIBEidJHCYPtXnDO1FMkq3pIEkwUqiTAIHAc70U/jsSsFxpamwSkydJHCDEDXnNqNdrRUVdWF6LbOzccynKbHlxovCYsDWuAX3hc71xQS4AFysqTrHAQZIERpaimO1ilbsJBzJ1Twm4sehv1FSHatOd8ya9NysjTsjtXcQk0NiG06gUi/Wa3GzlCQZ1IMKFwocYpcztt9KsqNBNiAo71id4CRebzF6tdr0Yu0bv78ynSb5OvwuAQvUxTpvsxIBbhc6enPlXL4DtcplDn2hhLylEKbybidJVztoQBY9Lmm/Y/t4w9u4hPcu5jkspCFJzboiSUmCnU3141rXbKmllkIWDg3doOd2E4DHdmegn5Vq7slQtBPkD7aV0OM2sUJDqnIRI3hdNyADujS4vTVOLBFtYnTWr9rnLkdoxXBw7Wx3CbJJ5iDW/wColE7yADaJsT5Gn+0dqOndQoDoBvECJVfhf40s+0YgSTKiRbNMDrGlOhianiLnRg9miYXs0wTvXVxBGh5GaOX2XYNiEk8gMo9uNLMA443JISZ/OtMht9MAFCs3K0e9SVas3tJgaFNLeKBF9im73HoNKGX2DRwcj0/vTNnauYxlUPIg/KtnccRoo02NfFL9QmVHDvmJzj3YZQNlgj1qg9i1/iT8T9K6lO2FDU/KrTtOeA9xTvbMUuWK9kwz6HKnsaI8aieQSB8zVCey6bzYj8RP0FdmnG9Ks+1jlND7diOrC9lodEefvdn0JBmb6EG3pIoFWx4Go+J+VejPLSowWz7KjytXJ/pE2E4vCqVhwsLAy5ALFKt1SoSJkA6/0pnvOpCN2rkWChJ2R57tDbaEBRSkqCTEjSeflrXH4zbDrmqoE+EaHzHGqcU6pIyREW0gxrcESLn4UIVmuJXx2Ir/AOZLbwWy+/U6tHCUqX5Vv4jbaG33XW0oUYA8WWROkTz0n1oRnDiDnJG7uiLkm4nkIv7VThdZPCI85GtWKUTes1WrOpK8ndjYwjBZYKyGGC2WvNIGaN8A6LSCDHqkExySrlRLqw66goGXMqIkHekEkgeG5PSI5GmWylHu2Ckgd2bGwOdYUJJuopmE/wA5PCAv2hhMiw4lRVmcOVSiB4MviKjGpMEnQXOtY82aTTKTu2h+MSo5AmUobUtbgEFGcpypNozAKzLjQSqdLadqGQcG5rKVBU3Ey4QZH3ZBCue6L2pScQW0NIKoUoHKrOIAUXE5zlMQc0dA3YmrNsbUCmUoClftGiVAnNB/ZrSmdYCRlnjJ6is6pPOmvEjotSRyE1mtctSumOPopOxVqnJmjjyHvXI9u2XGUhsTvEAkJsSdEg8SOIFdQO0qIgOtjycT8a4B13EOr/bLU+VLWG4JITClJsnS+UEAWiCa6vatedKlv12OTgKOpPboCIUhLmSd8phQSomTaUq5QVa304RRGOS1oDnWlKsqQpQEJgqkA2G6RwmT6xnFtpKtEuEqSZRBiLKKtFAagJte9DKjLlSRClE6qOckIEqlNhqTwtwNeSs3K+6O46d1Yvw7LhyrISFrGYwSmJvlAHi1M6CfajWkKSkyLgTlBUpRkbvPl+b1rhEPJcU043lcyqym67DdVAgAJEJ0sYkVsnGrGraCcwGdKhoARKgpM9bcongVVFJvhAaLuUF5biVF0wACkEaiQRmhB3eXOytKqwmy9/M33WbJZJUpKhOtkhUkgG/CimQXFurCoCU5UDxERO9vjdO9B1JjW99VOhJUCAArLCgbgpTlG7lEEEG2mt6u73jHbyAdKT6FnfkKDSG1G+8tIX4ucHVNvIwKNxmESgqzFSFneJIB7xI0iJ5ax8jGcL2nZZhKgrNHiKCDBGqeEWnjRO1O0eFdb1Wki4V3SyJixIESDxA1iecpyVL/AJWhzwv4d1JX8LoTLdbckQonKLTuiNFZUkEjnc8OdDstEKEyblIJgydCUog21sdAk+dDYvGIS6ptC0pjRSFWMdRJOvhNxJ01pizi21KBUqSUkeE5Qkm6VKFptMEAgZudaHBxXDMTjKXJQ9tdaFZA5ItIAI0vcWA468SPKtsNjhvd4SlCjaLDpZNyOEnlW2KKHYbJ7tSN4ExoIOYAiSBpYfKl+OwneqC2SVGN3TeBJmYmY1B66WooqLVnsRU5vgb4dpC1FSVZSAFCLZegve8keV6a7O27iWCUlwOCQAXDfKJgTPCTw4ik2EwENAKEKIUDBTdYB3jCoTpF9T53LfCAILYiwhUHjYEkzxJvyMaUqNWVOXcYSg7cD1HbVXfBLiB3REEA76b+IE2UI4GPOiVdqMKXEoLikqVAhSTCSQkjMoEgDe1+VcQphtLiTOUb26Qc2mpzTAiDbz6UXicG2uCGzKRbUFQkwBHjj4TbU1vpdpVadr7rzQmVJ+Z0OO7YttrWgJUrL4SmMqrJJvaLqjj5Vds7a7eJJCCQoXIOsWuL9fO1cRjcKlYyOZklJgxbIZ8REaQOPlxqhezVsIGVYcJiDBAiJ3QOsHjYdYGun2rUUrv5f3Bnh7d3qekLw54zWs+Y9CK4HZ+JxGGdD6Fd6kkwgElKkkHdj7pHi9Old/g+1OGcRmhSDfdUb2EkaRMyImbda6+H7To1F3tviZKtOcN0ro0JPBR9zWodX+I032btFDiA6jLlMxmGU2JGhvTL9ZjghHmZI+EVvVZtXirp+Yi667HLjEOaSr2/pSHtHiTh4cQwXVrzFWdbkJCUgTEwBfhGvWvSv1qk6pbPMT/WBXBfpZ7VNJw/2VtDClumTliWgkpUCYPiJ04WM8KRiKzUHdWG0LSqJLc8j2xji84VmFKVBUoApkwBEEnlwj5UCoXrdd7m96xFcRtt3Z2ErKxllUGt1OjSqtaylF6GxZ0GA2k2hCYBzSmw4m6Vg85TAA4FSqDx20UrRkyiU5QFXBUEApTN9YJnWZ4ZQKXwdAL/AJ51Ftc1AfE0tQSdy7IYbQQPs2FcGsOtL/iQ53g8tx1PtSxSzAvTDAYg5FtBrvUqIMEKMKTosZLgwSNYgnpEbw6gbJQn+Ik8ekmrTtsw7XF8dDWKeDMLd4j0QfrHyqVWcvKQtdPhVwU61lhwokZkwRaZAJ5HU8xRz228I3dOzlkaZnXFAT5FJEwOBrGG7ROKUpTOGwwAgCWysgRASkk2AieF5rqVcRGq1Fwv6mGMMizZregvSFPLSgrSTwB0MCRIGsxrROzmXyoFCLKI3u4S4BPEBSCBrNorfafavHhOUuIShcpKUtNAQRBElJPHnVDvajF51BWLxGUaZXVJ98kTWZ2vbIl9+gxPrmbDmNj41Ss6WnpyzmS0qPCDluAAQd08iDypt9gXKV4jGMARKkuuhK7CCkCJmeul5rhDiXXFArUt03nMpS5Jm5zTRTOzncqk5AM0mTax4Cg01OzUV8iSqxjzJ/Me7S7pgAM4lgqMZlIWVAxGhy5rmeFUN7UZDgJcQpHeJJGV1SgmYUfCkFZBPTypanYS4AWqwFo5TNvc+5opjs4DeT62+YotCUlZpfJCXi6UP1P5s6/YvaDDuJdaQ82yCpaYcU42HGMoQhC5cAK4K9JF9KL7Z7fWMKlBcwhQlLaG0NFOcDMiAkJdUoI/ZpNtcvQ1yWF2IlBsspP7uvuKsf2A2q4kkmSSBJ+NBHBtbAy7VpeLFGI2iVzmZaOgnfm3XP8AnpV7G3XUJyIQ2kWtCj6XV60wR2fT19f9jVw2K3oL+dhXRWFw7VnBGf3ok7qTEzm2XSQSlBvaQox5b1aja72ogeQIi0Wg2tXQo2CCPu35b30itF7DToFJnqADR+yYZf6aB97JveTEg24/EZvh86i9tPk5pTMATli0zblPT+lOm9lAGCJ6ACfSYq5eAZHiK56gf0qLCYXnTRH2q+E2zm1bUdVrlPOxPXnVrO23wCBl6HKZTb7sqtIkU0ewrXBKvOZ+AqotsDUqB67vzqezYXjIg12jN+IrXtR/MVFW8TdRkGY4wQK3G3HpG+LGeMHXW/WmqcOxb9pE8sp/8atxOEQBZQ8iAPWp7Jhn+hFe8pXS3+/gITth7XNxHAnTTU3rT9YLvuNniTlvJ4zmsfKKeoBBgJQfQf8A1v70anBlQukDyvUWFw38CKn2lKPP1EGB7Q4tlOVteUTJOUEm0fen+tXntftD/nkX/wCW1b/TV+N2EvUL9IH01pO7s1QMKUR/KSa0RjkWWKsvUuFalV32b9Bi721xo1xMcY7tr6pmuZx2OceWXHFFa1akx9LCs47DkGRfyoYSOlc7GVJyllfCN1GnCO8Uv5GRWRWuatrcz7f3rEaDdtuTAPmelFNNovdVv4f6VVg84XmQSkjlY1e2xvZT4jFrkkn+550EmEibRwiEpSUqVKjoSDbnIAjy61UypsGTFrCRmBPMzWcUuDlUneTa4yx5jSjMKRJUFNoB1OUEhPIgD6Xob2RfUoRihmASM15gCZPkfWrGlG+VJBBuCbpEm0a28qaYWFRlxCFEwMqP2ZJi43Ekq9BROI2bkcyLeB0IQO8W6RY5SidzU+I0GbewdthEcR/CPM3+NZp0/gG8ypdI3lAApbJgKIFyTNo/tpUolb7/APAHKzt9/UXbaZccAQlpdlTJSUjQjUxzobZmDcCgmUCVJBHeszqBYFckweVFt4FsCMpP8iQfipXworB4dGcZW1neSJBEAkwJ3OJ61snnbzGCnKEUoJXQNtnZC1PKQgkoSYTncQVeETIGW88hpGup2Z2AAJcKh/LHzpnjIQ+6240srChmIKZBKEEWKDz51nDOwYC3EdSgD4hf0qoK8VJisRKabjHZL0JhcOlCQlI1/Eef8NE97b7luYJ+c0M5jSk3xCT0lw/NMfGq3XSbpcR6qbHrvRTlO33/AGMDw7m7u/1/cdYFKnUuKSpsd2nMskgazCRN1EwbdOFDjFIi6vhFMux2KX9mxgKkaNEBK21DR2SrIYHDX+tctiO/Fzh5HMIUE+haIBpcMRJykn0Gz7Nioxab3uNHMW0YHHyNAuPKB3Z9I+dqXNbQQVeBQPRxMeykkj3orvM0ZVOgcgptQH+ofKnaj6oH2NQ6r4sMLzxA3tfU/CrcM8pMytJPLLB9zQicS2LFS/RAj0hyuo7FhtxnGCcyg2nxNIkSHPAorVFwJsNBflKmIVOOYqng5VXkSS+QE3i0g7xHuT/asOYxvUITx1hNYTsgnCNPJSpbi1HMEIlGWVwoFIJ0CTcCJIMUnfLSZQvXiDlt5iLU2NeMk7MzTwLhJKVxgNpJFwmfQH41s7tWfuGkjLiROXjym3sa3Ri1puFCNCTw850o1ULeFjfZDP7fybIPlPtBqv7YVCS2lUc5kD1ECgU41U3eH56xW3eA/wDH85Bn5TU1L9foTQS6fULUQUggewFulga2bWCnKSVHgLA/EUOhuf8Aig+UT6g1ctq0hVxxTAPsbUUZlPKtjV5yLlHxM+2lXYTaY0hXqTb3oTCtrWtKEuHeUBdMRJ1GW55wBRO3cGcO+tl1wLyRdJIBBAUDeDx40Oss2XqMeHzQu1dfEtffWTZcD+U1opKl6wr0T9DSxLgJhJMeU/WiUYjL94j3/IqOauKdJx4+hsvZCDqgjy/3pfi9gD7p9DTJjHlZyJlauAAJV6ACaJGKS3/jED9xJCl+RAs3/MQf3TSqk4PYZB4lbr7+ZzitjuNgKXhwtCgrISFJCiLSCIzgE3A6A0ucweuZKk2+6kRPLWwr1DGPd5g2AlhZGdwjIFKISW2TKiBEknUi/CAAKUK2cJuQnhBW1I91g/CuepKSudKderSla19kcChUa55/iP0p72awhf7wQEhttSgviVZVKSkkrAvCha8GbxTxOwmFkwouq/A0glc+ZsR6+tF7K2WrvChLS0nIpKUrhtO8DP7PxCeKys9TYUupHZtGzD4nO0n/AFOWaJEZmVEQAU5CUnSbiQehq5hkKbgsJbIlUBtXeKGlhIPr4ecTTH/0m81ZxtjDzxQ39oJ/6q1ADyPGrDsECVd+4vLcZ0hKExxyGUjzGnSgdJeJqjUY5wezy4Eu7rKVIT/hgIWuBF3I3RIMhMTckyTQzjMDI0VITM/sSGwLzqN5U8ZN+lEOM4hDaGfAlKQIClDN+9bn7VUWnZ0nnBn3m9KlJIbFXKvsyrw8sCSY7tgxJmJU2SdeJqVk94OC/YVKXqsPSiBjAlUkmeeWI8uA9PhVrOGCSClJzJ8JJEpI0Kcth5iKJCjroOulbglXM+Wn0Hxpmu3yK00Du4LvFS4oZiQSpajfqqJUffhpWUYRIVxIn7gyz/m/pV5UNJHz+AifjW8j87vyv70WuTTA17NCjJj+Yg/OK2GxGinwNyDdQEGORghMeQnrRaPL2H1qxKQfyT86JVmC6aNdm4Vtll9CU7zuUSCSIAX+I/vcK57/ANOXGULEcZSfawg11OS8f3q9vCyMyzkQDBUY19xRwnZt35Fzpp22Oac2U8IP2ly2gIKh1neI+FDfYMSfAplQGp7tA9ypqPia6ZeIaJytNqeVPGQNY8IufaOtHo2Ti8QRmbShKogEkAeaZM6feCqdqpcitFM89xTDv3sOy51QopF+YadSJroew5U0p3Nhg0lSASR3u9BNiVrUNFHSu5Z7GRZ1wk8kIEDqkrhI/wApptguxDSCVdyoykpJcdMkH+CAPQUFWopwcUHCGVpnlu02SrZ2HQ1H+ICO8U22QId1zKCSR0PCY5K0/rAWQ4pYA0S8HBHklZB9q9gf7HFa1pWhCGcikiCDGYZf2d5BgmD1ry7a/YQsYjuHMTkm6FBAyqEkC4XANrjgfMSdGSyu/ixVaneSa8AAHHkg/ZSRzVg0Kn+ZTVXPYbElO/gMwnUNOo9shR8vWg19l8qgFOkKmIKZvwEzxEH18p3X2Yg+MEcwkg+V5inZ4+ImVGfgbKwyjrgB/lxk/wDdqtxmP/4Uj0xc/wDeojDbHXEIetylYPpH9aqb2RirlOJTE6FxwkeYI06i3UUd0wVCXn/IqQkg2waPUYufi+RR7Tz0QlhKf4WAr/uBVDqaxaTlL06feX72FXJweKIkOhQ6OKPvIt60KlG+wudKq+UHbPcxQWN1SUXkJYS1aDYlCBamXblzELxjqgFd3CACtaUt+Aad7CTedDrNc1s5p4PIK1kglQO+SPCqLaRMCu22v2Ybcxa3HM60qykBpSAbBKFDKf8AEsCd1U6CDwQ2tdPy/c0KlPRtvz6dDim8S4m63MKn+VlZP/QQs8fnW/21KiEl1C1aw1hWVgjoXUpV65a9BwewtmonLh86hGbPvFNxBKHbgSfwjlBtOr+JS3AYLbY1gXT6i0egprqRXJUcM+Ti3MHiHBkGHdAIiMQ4Wmo5lCQ0B5T70zw/YrGxuuYVkxCQ2FZhbTvAguDQ3zK0punbLqAQSoJvMXF/MEihhiW16ADnc/7e9JlXS4NEcPflhz/ZlpTDX2pzvFNqUSA5dUoaSSVRmN0dNaEGDwLKsqMNHIrSl1Mx+IqKhx+7yq3B7UdbxDJQoKEwpC/CptSkN/dEeJSACRYlPM0ZtPZhLq8jJKSZHdSoXE2gG1+nwrPr5VsO0Yt7gZ2os7qChPQWPsoRwoZLzgM53f8AMUgeQG7V7mxnojunCDbwLHxGnwpanZGLClZQUoSY/bgNyYuEqUUiBIEkKm9VqykHkjEN7w+flE+sVhaM7ayTCAN5RvAJi0XKuQkGeIuQOXEohL6lOLP/AA8MO+PTMoItfQhKtKMWpDyYVKQggdy2sKIVac8E5Fc88q4QBEDpy5ZeZFReb7tlASuP2kQs7qUuLAJBBEkgaEAybVV3iCrKlp51RkhLZRmgcSCNLRPM0wZw0NJjKgFbkShLhjOYAz2jXrpejmmXQkhTZynUJyIJ0iEmEJ85NFTppq7BcrC1tYi7TSei3HCofxFolE+Xw0qU2RsZCgD9ldVI18XxSCD6VmmaUQc7ONJH4R5km/nVC8Qi4zA8xIHvH1pKpRUY9hr6qP5/pa2yIKj4Rqec2gc6zKj5js4f9rQLAi+gHH2186IQqdL/ABpQhN5jePUHKn8j1PlRDKFOLShtJJJASL+InKD/AH5STYg0WkTONsPh1rMJSVHlr8tabYXYTp8UInTMcnzua7LY+yMMyyltSitQAzmE7ytST3kkXNhNhA60xRkTdponzUsdPugg0jUXQjbOSZ7Oc3FAW/w21K9cxNvVNMsN2ZYzAltTp4KUsrNosU2Tx0g097xaj4G09coJHxn4Vo8VXBcXxskAf/ICh1JPgo1w6O7EJbCE8gnu9I5QCahxR+/AF/Hlj3A+tYGz1KE5FFP3itZHPooH0mtk4FhF5ZniIzkeqT9KlpkvE0VtdPhDqR0RKxb92/HyqwbamAC6o8wjKP8AUU1U+sTCCYH3Utx7FaRehHMPJ3gtXRSzx0lI1I/IqvxPEndC1bXVMlP/AFXEpI9Am/vSLtMtGJRkWprNOZCkJW4QrkVC2UixH9BTVOFUnwIbTNoASk+oJmfT0q7C7LcJhSlgcMkJuepj4dBRRVTxI8h5kpYjItELFriCP3FfQ9R0nIxaSIiCBGbn0UPrXYdp+ya3klSEkPIEjMrxgWyGTExofeAbcEHCSJSUucQoRmHUHQ6jzHnT+81sUnE1deyrE2PAzqOYP1qxx7MZuFC+bn5/1FXHK4NJHTVJ+nyqgsQQOGgj6cjRRqSRHGJu2+fvC/OOBtNuHUc9K0bStF21m82mOEHKRrbhWhUUeIFSfiOvTz+VE4doKnKu2om5kCQCOGkTb2E0TUnvcq6RsMchzxtALnxAATa4IiCZvYg/CtVFwXbUIHnH9vIitnbgd4gQbBY1nodDbn8arVhVp3m3CocCNecKH9aW1JPcK6Lv1qZh0SeBJuPI+da4jaC8pWgd5+7MKjSxPiI5GKw0+CAHUDXxCfcgX9qn2NBulcCddR6dRyMedXt1ITZm1WXgAFpChqlcpUDMfA8TFFLUkm5PTdMxxAUkX8zXOubMzSq4UFuZXUWVZatUzfyPvVTHaF1o5H7okp70JlPLeQLgm1wfSm6Kl+UDNbk6AkBRUJgdyN4Ea4vDrFyAVeDWmDjTyjKcOpabQoBXIX3Eqt5xQmAWHGHVSlW9hUpKLpMvqVIkkg7kfy07e2YV2hLuUCUqcUsJypE7qjuRyQRpcXqowu7MjlbcUr2dnOVb7fVJUlJJ5ENFSz5EAHlV5w7TZCVFyBAGVGRFv34JCeEFKSOdMV7AS6P8NsA3icqgeXn8etTCbCSlUd47l/A5CkjlBIzJ9CTApumugOfxM7PUlIKWwhAOuUKhc2u7vKWf4iRReG2OCoLLLTmW4UEBa0eTiCCk6UQUltOd5aWG0wO8cOVMG8JzA8K4/bn6TGGyRg2g8sWDziVIbHVDc5lanXKLAwdKLgC56KgIS0FKCWUJkZlkZbqJjMqJkk7ok9K5/tZ2sRgChttBW44gOBawUICSpQEt6lVjZUROleP4rbz+KxLTj7q3FBxMZ1SEjjlSLJnUwAJrpv0vYqcXhyk2+yo0MQe8drFOtKOIhSS2abfwCnBypSknuYf7XYlaio4t4E8EuKQPRKCEpHQCsVxgdTxJ/wAv/wCqlbDkvCy/iY9dUBpOWd5Z4nl5f2Fbl7NvEQBGVP18zx5DTjNTi0m4GVIFhrAHH4W/2rTNAkCSbJHLpy8/7TSztFwd9Y8RmNdB0nQcgDyr0L9G+yZnFuJBTJS0kkX1SteWCf3R0C+lcb2d2P8Aanm2EqgSS45+7O8oWtaEjqU9a90ZYaYQltCQkJACUySIAgQBcAenC9KqysrIssSIgpSkA8Yj4boqxOabJT6JgH4Kv6itmFOfg/0iD5Ek/Osvkq8eVKUx4ljXpA+tIjEq5a2p3SD58PnQ5ciQXCVcQm5/0gRQy8KFwM646EH2JzEn2q9jZukiY0zqUoj2MfCm7AlD/d6khRJ4jMBI9b+XtVrcAboJHEZDl+IT8abNsgCyQD0AFDF/L4QpRH4QpXsVEBVWoJFZgV3EtwAUtiY1Wn4AAzVriU5cxTlSm5MBIjjdZFupFVd+4SFd0UknVcJUBzMW56KNFMhU71+oKzOtyCYFF6lC5raWHI3H2ylWmVbaj5CFQeNoNFF9MQlLirWMZQOsgA/CrcTj0JP3c3mFK9QN6KXLxjoF1KIOkBIEeZuPY1G0i0rmFPuL0hF+CCD6qWPeAK4ztl2ZCyX2zLqblIklwCN8QTvjlxAjUCeif2ggTmUkmJ/GfQmAZ8qE/WhPgST5wBy8IEfGfagUnyFY4DCJDqFONrT3oGYpMBK0ADebI48xpEEdB0uZvDaPEOR69Kb9otmLJW+2iCZUpKTYHisCbTcqvrvc65guEEKmFfmxptk90WmOWnUiCRmTxExbiDa3nS+ClUp3b28uaf7/AAqM4kKkiyuI+fl58a3BkGL9BqOo+o/IidiNGVYkOCFbquM+EnqPum/y10qIQtCoBM8BNyByJ8Q6GpjXmihJCSHeK5lJHARb88aobxKkbqhKdf7g8I5fKr2ZQQp6SQq3pp/EOHnx1qNtqSc6FKTI1GhBtroR0PtxpgcQh1Izi4EBxAIUI0zgeLzHISDrSXbSnUNxmlKpCSCSklyEzy5GeOW/Chy3ewWbxL9jPpU2mSU5pVzG+SvT+bhVDezSpDjgG6HHEk6iyimDPC3lUKkhPh3QLi3AcDx8jTPshhXThi4242QBJZc+9m3lBJuUkE2kETTopK7AbfAZ2H2UW2FlLfeD7Xh1lKSmSEJdKsuYieFutHuOsOYl1SwtDhJgJPcOJnQEKBCvXURu8x8HiU/ZUQVMrViFwN0KJQ0jNluAsftYsRPSgNt45bLKnkIaWpEd27lCgDmAIUCJSqDOVVp0nU3a7zAvwO8wTDpSVOqUlP8AzlpSJgTC2zOfQQUwDPhnVN+kPtSvBMNLwyEqLsoBcbVuOCCVpkwTwAjWSSrSuZ/RdtJ5915bzq3VkxKyTbdsmdB0ERTn9M+KQE4UFd8zxSdUmO6F/efSuPLtCo8csNbb67XHugo0lPxPJNqbaexTneYh1bqzxWZjnlAskW0AAqhaRlPEjlp5mb/CjFYQurgBsEnxCbybSDNv4RNYw+x1SCTCZglQUm8SUmxCTE62rrmTYCwaiHGzyUK6Ptw8pbrZ4dykGDacyz73ql3YS0LQd0JBCiSRIExeOvDWIrHadeZxslBSS0IEgxC3AeHGAY4TWWaviIvyZpg7YefqhHNSrRUrSYsw6y5iDeAfdQMH0B+N+VZZ8QUONh5aHyJgjyHWpUqjWey/o97M93h0ubveOpSuTcJQZKAEgRoST1UeQrsWMHEjMDzEQB5XNSpWNtt/Epme5SkHeWbXEn6/1qnBFpcltuYMFRjUed6xUooxTZb4uMe7g6ARxH+1RKbzmJHI1KlVKVpZUD0NZAUQJzHqT87VSh8wSJME6wNLE2F9ONSpV7+JaEeK23AusyDolN/LetQCtqlRsCro4cwvxi3TjxrNSoltcs0xTj/4ggHgm3DhlFp862c2evLvqVBIAE6lXA8T71KlCpBWL07GUDKwhMAXNyUgWiJkwOMVh9hpsbxUQZ5jlEhJkC3M61KlVOTUrEirq4uc22tIAQAkcCAJibR+Zrz7tBgQ2c6PATEaZTew6axyiNIrFSn09mUxUEKkFNjEj+hplhn5HeAXkTeLkGP9/epUq5sKJc/hxGdNwdU6X0kcjPpVAcEc0Hz0+dSpQJlmuIYU1C0qkGfNI68CL/nWsYPEB7EAKEd2nORchS1ApB9E5r63HKpUrVTEyKtv4PIhSE2UspRHDfOWQfUnhppVMQCUkoIidPzpUqU6yat6gXsxzi3UnC4dLouVPmRFrto0/wDb+FBvtONsP5XCptWHckHWAkqTBN7KSD6VKlLRbZv+iBULWP4j7BP1o39OCcpwiDqEunnMlofQ1KleZ/3dej/6s3zf+HS8v3PNGcctNpkdRNtInWOk0cjbC4jhy4Hpe9SpXpjmF52wMiQE5VgzmBJkRABBJFgBf0qYjaAcTlUOogRf09bVKlC4q9ynwCpZT+KP5ZqVKlQTc//Z");
*/



        call = apiDetailCagar.postImage(uploadImageBody);
        call.enqueue(new Callback<DetailCagarResponse>() {
            @Override
            public void onResponse(Response<DetailCagarResponse> response, Retrofit retrofit) {
                dialog.dismiss();
    //            Toast.makeText(AddImgResourceActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                if (2 == response.code() / 100) {
                    // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                    if (response.body().getSuccess() == true) {
                        Toast.makeText(AddImgResourceActivity.this, "Berhasil menambahkan gambar", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                        Toast.makeText(AddImgResourceActivity.this, "Gagal menambahkan gambar", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddImgResourceActivity.this, response.message()+" Gagal menambahkan gambar", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(AddImgResourceActivity.this, "Terjadi kesalahan jaringan", Toast.LENGTH_SHORT).show();
                // textNamaCagar.setText(t.getMessage()+"");

            }
        });

    }

    @OnClick(R.id.btnSelectImage)
    void selectImage(View v)
    {
        selectImage();
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        return imgString;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_img_resource);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiDetailCagar = ServiceGenerator.createService(ApiDetailCagar.class);
        Bundle b = getIntent().getExtras();
        id_cagar=b.getString("id_cagar");


        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        //=b.getString("deskripsi");

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent();
            setResult(1, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.example.herlin.projectplbtw.cagarbudaya.userinterface;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.herlin.projectplbtw.OnLoadMoreListener;
import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.addherritage.userinterface.AddHerritageActivity;
import com.example.herlin.projectplbtw.cagarbudaya.adapter.RecyclerAdapterPosted;
import com.example.herlin.projectplbtw.cagarbudaya.api.ApiCagarList;
import com.example.herlin.projectplbtw.cagarbudaya.model.CagarListResponse;
import com.example.herlin.projectplbtw.cagarbudaya.model.CagarModel;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PostedHerritageActivity extends Fragment {

    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";

    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";

    private String id_user;
    View rootview;
    public static final String API_KEY = "asdfghjkl";
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    //  @Bind(R.id.contentCagarList)
    //  LinearLayout contentCagarList;

    @Bind(R.id.listCagar)
    RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.fab)
    FloatingActionButton fab;

    @OnClick(R.id.fab)
    void addCagarBudaya(View view) {
        Intent i = new Intent(getContext().getApplicationContext(), AddHerritageActivity.class);
        startActivity(i);
    }

    private List<CagarModel> cagarModelList;
    private ApiCagarList apiCagarList = null;
    private RecyclerAdapterPosted adapter;

    private int currentPage;
    private int totalPages;
    private final static int PAGE_SIZE = 10;

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        currentPage = 1;
        totalPages = 0;
        getCagarList(true);
        Snackbar.make(coordinatorLayout, "Sedang memperbarui data cagar budaya", Snackbar.LENGTH_LONG).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        rootview = inflater.inflate(R.layout.activity_posted_herritage, container, false);
        //    getActivity().setTitle("Posted Herritage List");
        ButterKnife.bind(this, rootview);
        currentPage = 1;
        totalPages = 0;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity().getApplicationContext()));

        progressBar.setVisibility(View.VISIBLE);

        apiCagarList = ServiceGenerator
                .createService(ApiCagarList.class);

        sharedPreferences = getActivity().getSharedPreferences(sharedPreferenceName, 0);
        String role = sharedPreferences.getString(roleMarker, "0");
        id_user = sharedPreferences.getString(idUserMarker, "");
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter == null) {
                    progressBar.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext().getApplicationContext(), currentPage + "on Refresh Lisrener" , Toast.LENGTH_SHORT).show();

                }
                currentPage = 1;
                getCagarList(false);
            }
        });
//        getCagarList(true);

        return rootview;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private void getCagarList(boolean pBar) {

        if (pBar)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);

        Call<CagarListResponse> call = null;
        if (sharedPreferences.getString(roleMarker, "").equalsIgnoreCase("1")) {
            call = apiCagarList.getHerritageList(currentPage + "", PAGE_SIZE + "", API_KEY, "pending", id_user);
        } else {
            call = apiCagarList.getHerritageList(currentPage + "", PAGE_SIZE + "", API_KEY, "pending", "");
        }

//        call = apiReward.getHerritageList();
        call.enqueue(new Callback<CagarListResponse>() {
            @Override
            public void onResponse(Response<CagarListResponse> response,
                                   Retrofit retrofit) {

                if (2 == response.code() / 100 && response.isSuccess()) {
                    showCagarList(response);
                } else {
                    showErrorMessage();
                    adapter.setLoaded();

                }
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                //         contentCagarList.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan..",
                        Snackbar.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                //        contentCagarList.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showErrorMessage() {
        Snackbar.make(coordinatorLayout, "Gagal memperbarui data cagar budaya", Snackbar.LENGTH_LONG).show();
    }

    private void showCagarList
            (Response<CagarListResponse> response) {

        final CagarListResponse cagarListResponse = response.body();

        totalPages = cagarListResponse.getTotalPages();
        cagarModelList = cagarListResponse.getListCagar();
//        Toast.makeText(getContext().getApplicationContext(), cagarListResponse.getListCagar().size()+"a", Toast.LENGTH_SHORT).show();
        //  if (cagarModelList != null) {
        adapter = new RecyclerAdapterPosted(cagarModelList, recyclerView);
        adapter.setCoordinatorLayout(coordinatorLayout);
        if (getActivity() != null) {
            adapter.setContext(getActivity().getApplicationContext());
            adapter.setInflater(getActivity().getLayoutInflater());
            adapter.setPostedHerritageActivity(this);
            recyclerView.setAdapter(adapter);

            if (totalPages != currentPage) {
                adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {
                        if (cagarModelList.get(cagarModelList.size() - 1) != null) {
                            cagarModelList.add(null);
                            adapter.notifyDataSetChanged();
//                            adapter.notifyItemInserted(cagarModelList.size() - 1);
                        }

                        Call<CagarListResponse> call = null;
                        currentPage++;
//                    Snackbar.make(coordinatorLayout, "Page"+currentPage, Snackbar.LENGTH_LONG).show();

                        if (sharedPreferences.getString(roleMarker, "").equalsIgnoreCase("1")) {
                            call = apiCagarList.getHerritageList(currentPage + "", PAGE_SIZE + "", API_KEY, "pending", id_user);
                        } else {
                            call = apiCagarList.getHerritageList(currentPage + "", PAGE_SIZE + "", API_KEY, "pending", "");
                        }

                        call.enqueue(new Callback<CagarListResponse>() {
                            @Override
                            public void onResponse(Response<CagarListResponse> response,
                                                   Retrofit retrofit) {
                                cagarModelList.remove(cagarModelList.size() - 1);
                                adapter.notifyItemRemoved(cagarModelList.size());
                                if (2 == response.code() / 100) {
                                    loadMoreCagar(response);
                                    Snackbar.make(coordinatorLayout, response.body().getListCagar().size()+" "+currentPage, Snackbar.LENGTH_LONG).show();
                                } else {
                                    currentPage--;
                                    showErrorMessage();
                                    adapter.setLoaded();
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                currentPage--;
                                cagarModelList.remove(cagarModelList.size() - 1);
                                adapter.notifyItemRemoved(cagarModelList.size() + 1);
                                Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan..",
                                        Snackbar.LENGTH_LONG).show();
                                adapter.setLoaded();
                            }
                        });
                    }
                });
                adapter.setLoaded();
            }
        }
    }

    private void loadMoreCagar
            (Response<CagarListResponse> response) {
        CagarListResponse cagarListResponse = response.body();
        if (0 == cagarListResponse.getListCagar().size()) {
            currentPage--;
            Snackbar.make(coordinatorLayout, "Tidak ada data cagar budaya lagi"+currentPage, Snackbar.LENGTH_LONG).show();
        } else {

            cagarModelList.addAll(cagarListResponse.getListCagar());
            adapter.notifyDataSetChanged();
            /*
            for (CagarModel cagarModel :
                    cagarListResponse.getListCagar()) {
                cagarModelList.add(cagarModel);
                adapter.notifyItemInserted(cagarModelList.size());
            }
            */
        }
        adapter.setLoaded();
    }
}

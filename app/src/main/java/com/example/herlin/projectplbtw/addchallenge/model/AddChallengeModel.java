package com.example.herlin.projectplbtw.addchallenge.model;

/**
 * Created by VAIO on 5/21/2016.
 */
public class AddChallengeModel {
    private String nama_challenge;
    private String deskripsi;
    private String poin_reward;


    public String getNama_challenge() {
        return nama_challenge;
    }

    public void setNama_challenge(String nama_challenge) {
        this.nama_challenge = nama_challenge;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getPoin_reward() {
        return poin_reward;
    }

    public void setPoin_reward(String poin_reward) {
        this.poin_reward = poin_reward;
    }
}

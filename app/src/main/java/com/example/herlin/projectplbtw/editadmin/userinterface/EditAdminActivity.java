package com.example.herlin.projectplbtw.editadmin.userinterface;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.editadmin.api.ApiEditAdmin;
import com.example.herlin.projectplbtw.editadmin.model.EditAdminModel;
import com.example.herlin.projectplbtw.editadmin.model.EditAdminResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class EditAdminActivity extends AppCompatActivity {
    private String nama;
    private String email;
    private String telepon;
    private String id_user;
    private ApiEditAdmin apiEditAdmin = null;


    public static final String API_KEY = "asdfghjkl";

    @Bind(R.id.textNamaAdmin)
    EditText textNamaAdmin;
    @Bind(R.id.textEmail)
    EditText textEmail;
    @Bind(R.id.textTelepon)
    EditText textTelepon;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnEditAdmin)
    Button btnEditAdmin;
    @OnClick(R.id.btnCancel)
    void setOnClickBtnCancel(View view)
    {
        finish();
    }
    @OnClick(R.id.btnEditAdmin)
    void setOnClickBtnEditAdmin(View view)
    {
        if(textNamaAdmin.getText().toString().isEmpty()||textEmail.getText().toString().isEmpty()||textTelepon.getText().toString().isEmpty())
        {
            Snackbar.make(view, "Tolong isikan data yang kosong!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        else
        {
            final ProgressDialog dialog = new ProgressDialog(EditAdminActivity.this);
            dialog.setMessage("Mohon tunggu...");
            dialog.setCancelable(false);
            dialog.show();
            Call<EditAdminResponse> call = null;

            EditAdminModel editAdminModel = new EditAdminModel();
            editAdminModel.setApi_key(API_KEY);
            editAdminModel.setNama(textNamaAdmin.getText().toString());
            editAdminModel.setEmail(textEmail.getText().toString());
            editAdminModel.setTelepon(textTelepon.getText().toString());

            call = apiEditAdmin.editAdmin(id_user,editAdminModel);

            call.enqueue(new Callback<EditAdminResponse>() {
                @Override
                public void onResponse(Response<EditAdminResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
               //     Toast.makeText(EditAdminActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                            finish();

                        else
                            Toast.makeText(EditAdminActivity.this, "Gagal mengubah admin", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditAdminActivity.this, "Gagal mengubah admin", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(EditAdminActivity.this, "Terjadi kesalahan jaringan", Toast.LENGTH_SHORT).show();
               }
            });
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_admin);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        apiEditAdmin = ServiceGenerator.createService(ApiEditAdmin.class);

        Bundle b = getIntent().getExtras();
        id_user=b.getString("id_user");
        nama=b.getString("nama");
        email=b.getString("email");
        telepon=b.getString("telepon");

        textNamaAdmin.setText(nama);
        textEmail.setText(email);
        textTelepon.setText(telepon);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}

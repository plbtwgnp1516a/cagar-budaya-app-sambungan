package com.example.herlin.projectplbtw.admin.api;

import com.example.herlin.projectplbtw.admin.model.AdminListResponse;
import com.example.herlin.projectplbtw.admin.model.AdminResponse;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by VAIO on 6/3/2016.
 */
public interface ApiAdmin {
    @GET("user/{page}/{size}")
    Call<AdminListResponse>
    getAdminList(@Path("page") String page, @Path("size") String size, @Query("api_key") String apiKey);

    @DELETE("user/{id_user}")
    Call<AdminResponse> deleteAdmin(@Path("id_user") String id, @Query("api_key") String apiKey);
}

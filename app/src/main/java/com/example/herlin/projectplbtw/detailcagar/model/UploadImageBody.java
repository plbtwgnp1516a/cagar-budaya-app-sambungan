package com.example.herlin.projectplbtw.detailcagar.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by VAIO on 6/7/2016.
 */
public class UploadImageBody {
    private String api_key;
    private String id_cagar;
    private String nama_multimedia;
    private String tipe;
    private String id_user;
    private String image_base64;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getId_cagar() {
        return id_cagar;
    }

    public void setId_cagar(String id_cagar) {
        this.id_cagar = id_cagar;
    }

    public String getNama_multimedia() {
        return nama_multimedia;
    }

    public void setNama_multimedia(String nama_multimedia) {
        this.nama_multimedia = nama_multimedia;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getImage_base64() {
        return image_base64;
    }

    public void setImage_base64(String image_base64) {
        this.image_base64 = image_base64;
    }
}

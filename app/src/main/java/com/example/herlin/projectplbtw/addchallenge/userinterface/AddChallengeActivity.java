package com.example.herlin.projectplbtw.addchallenge.userinterface;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.addchallenge.api.ApiAddChallenge;
import com.example.herlin.projectplbtw.addchallenge.model.AddChallengeModel;
import com.example.herlin.projectplbtw.addchallenge.model.AddChallengeResponse;
import com.example.herlin.projectplbtw.addherritage.userinterface.AddHerritageActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AddChallengeActivity extends AppCompatActivity {


    private ApiAddChallenge apiAddChallenge = null;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.textNamaChallenge)
    EditText textNamaChallenge;
    @Bind(R.id.textDeskripsi)
    EditText textDeskripsi;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnAddChallenge)
    Button btnAddChallenge;


    @OnClick(R.id.btnCancel)
    void backToMenu(View view) {
        this.finish();
    }
    @OnClick(R.id.btnAddChallenge)
    void setOnClickBtnAddChallenge(View view)
    {
        if(textNamaChallenge.getText().toString().isEmpty()||textDeskripsi.getText().toString().isEmpty())
        {
            Snackbar.make(view, "Tolong isi data yang kosong!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        else
        {
            final ProgressDialog dialog = new ProgressDialog(AddChallengeActivity.this);
            dialog.setMessage("Mohon tunggu...");
            dialog.setCancelable(false);
            dialog.show();
            Call<AddChallengeResponse> call = null;

            AddChallengeModel addChallengeModel = new AddChallengeModel();
            addChallengeModel.setNama_challenge(textNamaChallenge.getText().toString());
            addChallengeModel.setDeskripsi(textDeskripsi.getText().toString());
            addChallengeModel.setPoin_reward("0");
            call = apiAddChallenge.postChallenge(addChallengeModel);

            call.enqueue(new Callback<AddChallengeResponse>() {
                @Override
                public void onResponse(Response<AddChallengeResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                //    Toast.makeText(AddChallengeActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                            moveToMenu();

                        else
                            Snackbar.make(coordinatorLayout, "Gagal menambahkan Tantangan", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                    } else {
                        Snackbar.make(coordinatorLayout, "Gagal menambahkan Tantangan", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan..", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                   // textNamaCagar.setText(t.getMessage()+"");

                }
            });
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_challenge);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    //    setTitle("Add Challenge");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiAddChallenge = ServiceGenerator.createService(ApiAddChallenge.class);

    }

    private void moveToMenu()
    {
        Toast.makeText(AddChallengeActivity.this, "Berhasil Menambahkan Tantangan!", Toast.LENGTH_SHORT).show();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

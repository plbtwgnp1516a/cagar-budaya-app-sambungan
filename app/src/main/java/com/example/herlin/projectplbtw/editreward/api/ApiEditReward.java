package com.example.herlin.projectplbtw.editreward.api;

import com.example.herlin.projectplbtw.editreward.model.EditRewardModel;
import com.example.herlin.projectplbtw.editreward.model.EditRewardResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by VAIO on 6/1/2016.
 */
public interface ApiEditReward {
    @PUT("rewards/{id_reward}")
    Call<EditRewardResponse> editReward(@Path("id_reward") String id_reward,
                                               @Body EditRewardModel editRewardModel);
}

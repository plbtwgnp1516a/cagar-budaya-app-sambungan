package com.example.herlin.projectplbtw.editherritage.api;

import com.example.herlin.projectplbtw.editherritage.model.EditHerritageModel;
import com.example.herlin.projectplbtw.editherritage.model.EditHerritageResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by VAIO on 6/1/2016.
 */
public interface ApiEditHerritage {
    @PUT("cagarbudaya/{id_cagar}")
    Call<EditHerritageResponse> editHerritage(@Path("id_cagar") String id_cagar,
                                              @Body EditHerritageModel editHerritageModel);
}

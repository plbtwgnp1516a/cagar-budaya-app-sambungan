package com.example.herlin.projectplbtw.reward.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by VAIO on 6/4/2016.
 */
public class GetPoinResponse {

    @SerializedName("content")
    private ArrayList<GetPoinModel> listPoin = new ArrayList<>();

    public ArrayList<GetPoinModel> getListPoin() {
        return listPoin;
    }

    public void setListPoin(ArrayList<GetPoinModel> listPoin) {
        this.listPoin = listPoin;
    }
}

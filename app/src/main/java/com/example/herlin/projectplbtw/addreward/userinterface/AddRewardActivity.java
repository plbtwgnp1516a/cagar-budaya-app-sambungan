package com.example.herlin.projectplbtw.addreward.userinterface;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.addreward.api.ApiAddReward;
import com.example.herlin.projectplbtw.addreward.model.AddRewardModel;
import com.example.herlin.projectplbtw.addreward.model.AddRewardResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AddRewardActivity extends AppCompatActivity {

    private ApiAddReward apiAddReward = null;
    public static final String API_KEY="asdfghjkl";
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.textNamaReward)
    EditText textNamaReward;
    @Bind(R.id.textDeskripsi)
    EditText textDeskripsi;
    @Bind(R.id.textMinimalPoin)
    EditText textMinimalPoin;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnAddReward)
    Button btnAddReward;

    @OnClick(R.id.btnCancel)
    void backToMenu(View view) {
        this.finish();
    }
    @OnClick(R.id.btnAddReward)
    void setOnClickBtnAddReward(View view)
    {
        if (textMinimalPoin.getText().toString().isEmpty() || textDeskripsi.getText().toString().isEmpty() || textNamaReward.getText().toString().isEmpty()) {
            Snackbar.make(view, "Tolong isi data yang kosong!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
        else
        {
            final ProgressDialog dialog = new ProgressDialog(AddRewardActivity.this);
            dialog.setMessage("Mohon tunggu...");
            dialog.setCancelable(false);
            dialog.show();
            Call<AddRewardResponse> call = null;

             AddRewardModel addRewardModel = new AddRewardModel();
             addRewardModel.setDeskripsi(textDeskripsi.getText().toString());
             addRewardModel.setNama_reward(textNamaReward.getText().toString());
             addRewardModel.setMinimal_poin(textMinimalPoin.getText().toString());

            call = apiAddReward.postReward(addRewardModel);

            call.enqueue(new Callback<AddRewardResponse>() {
                @Override
                public void onResponse(Response<AddRewardResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                  //  Toast.makeText(AddRewardActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                            moveToMenu();

                        else
                            Snackbar.make(coordinatorLayout, "Gagal menambahkan hadiah", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                    } else {
                        Snackbar.make(coordinatorLayout, "Gagal menambahkan hadiah", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan..", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    //     textNamaCagar.setText(t.getMessage()+"");

                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reward);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
     //   setTitle("Add Herritage");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiAddReward = ServiceGenerator.createService(ApiAddReward.class);
    }
    private void moveToMenu()
    {
        Toast.makeText(AddRewardActivity.this, "Berhasil menambahkan hadiah!", Toast.LENGTH_SHORT).show();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

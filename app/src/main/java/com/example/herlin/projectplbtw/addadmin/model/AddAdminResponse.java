package com.example.herlin.projectplbtw.addadmin.model;

/**
 * Created by VAIO on 6/3/2016.
 */
public class AddAdminResponse {
    private Boolean Success;
    private String Info;

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }
}

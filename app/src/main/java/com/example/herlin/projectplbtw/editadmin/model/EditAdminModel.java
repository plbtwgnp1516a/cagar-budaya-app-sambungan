package com.example.herlin.projectplbtw.editadmin.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by VAIO on 6/3/2016.
 */
public class EditAdminModel {
    private String api_key;
    private String nama;
    private String email;
    private String telepon;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }
}

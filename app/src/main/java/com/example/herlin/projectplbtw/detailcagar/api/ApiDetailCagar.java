package com.example.herlin.projectplbtw.detailcagar.api;

import com.example.herlin.projectplbtw.cagarbudaya.model.CagarModel;
import com.example.herlin.projectplbtw.detailcagar.model.AcceptBody;
import com.example.herlin.projectplbtw.detailcagar.model.DetailCagarResponse;
import com.example.herlin.projectplbtw.detailcagar.model.UploadImageBody;
import com.example.herlin.projectplbtw.detailchallenge.model.DetailChallengeResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by VAIO on 5/28/2016.
 */
public interface ApiDetailCagar {

    @DELETE("cagarbudaya/{id}")
    Call<DetailCagarResponse> deleteCagar(@Path("id") String id,@Query("api_key") String apiKey);

    @PUT("cagarbudaya/updatestatus/{id_cagar}")
    Call<DetailCagarResponse> acceptCagar(@Path("id_cagar") String id_cagar, @Body AcceptBody acceptBody);




    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST("cagarbudaya/image")
    Call<DetailCagarResponse> postImage(@Body UploadImageBody uploadImageBody);

}

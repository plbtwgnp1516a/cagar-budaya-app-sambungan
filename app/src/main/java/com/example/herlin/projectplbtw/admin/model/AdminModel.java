package com.example.herlin.projectplbtw.admin.model;

/**
 * Created by VAIO on 6/3/2016.
 */
public class AdminModel {
    private String id_user;
    private String nama;
    private String email;
    private String telepon;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }
}

package com.example.herlin.projectplbtw.register.userinterface;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.login.userinterface.LoginActivity;
import com.example.herlin.projectplbtw.register.api.ApiRegister;
import com.example.herlin.projectplbtw.register.model.RegisterModel;
import com.example.herlin.projectplbtw.register.model.RegisterResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterActivity extends Activity {

    public static final String API_KEY = "asdfghjkl";
    private ApiRegister apiRegister = null;

    private static final String TAG = RegisterActivity.class.getSimpleName();
    @Bind(R.id.btnRegister)
    Button btnRegister;
    @Bind(R.id.btnLinkToLoginScreen)
    Button btnLinkToLogin;
    @Bind(R.id.textName)
    EditText textName;
    @Bind(R.id.textEmail)
    EditText textEmail;
    @Bind(R.id.textPassword)
    EditText textPassword;
    @Bind(R.id.textTelepon)
    EditText textTelepon;

    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        apiRegister = ServiceGenerator.createService(ApiRegister.class);

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String name = textName.getText().toString();
                String email = textEmail.getText().toString();
                String password = textPassword.getText().toString();
                String telepon = textTelepon.getText().toString();

                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
                    registerUser(name, email, password, telepon);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Data tidak boleh kosong!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                moveToLogin();
            }
        });
    }

    private void registerUser(String name, String email, String password, String telepon) {
        final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        Call<RegisterResponse> call = null;

        RegisterModel registerModel = new RegisterModel();
        registerModel.setApi_key(API_KEY);
        registerModel.setEmail(email);
        registerModel.setTelepon(telepon);
        registerModel.setPassword(password);
        registerModel.setNama(name);
        registerModel.setNama_role("user");
        registerModel.setPoin("0");
        call = apiRegister.postRegister(registerModel);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Response<RegisterResponse> response, Retrofit retrofit) {
                dialog.dismiss();
               if (2 == response.code() / 100) {
                    if (response.body().getSuccess() == true) {
                        Toast.makeText(RegisterActivity.this, "Berhasil menambahkan user baru", Toast.LENGTH_SHORT).show();
                        moveToLogin();
                    } else
                        Toast.makeText(RegisterActivity.this, "Register gagal", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(RegisterActivity.this, "Register gagal", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(RegisterActivity.this, "Terjadi kesalahan jaringan", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void moveToLogin() {

        Intent i = new Intent(getApplicationContext(),
                LoginActivity.class);
        startActivity(i);
        finish();
    }

}

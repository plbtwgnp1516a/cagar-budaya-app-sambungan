package com.example.herlin.projectplbtw.editchallenge.api;

import com.example.herlin.projectplbtw.editchallenge.model.EditChallengeModel;
import com.example.herlin.projectplbtw.editchallenge.model.EditChallengeResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by VAIO on 6/1/2016.
 */
public interface ApiEditChallenge {
    @PUT("challenge/{id_challenge}")
    Call<EditChallengeResponse> editChallenge(@Path("id_challenge") String id_reward,
                                              @Body EditChallengeModel editChallengeModel);
}

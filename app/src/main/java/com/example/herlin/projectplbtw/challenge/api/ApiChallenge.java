package com.example.herlin.projectplbtw.challenge.api;

import com.example.herlin.projectplbtw.challenge.model.ChallengeResponse;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by VAIO on 5/17/2016.
 */
public interface ApiChallenge {
    @GET("challenge/{page}/{size}")
    Call<ChallengeResponse>
    getChallengeList(@Path("page") String page, @Path("size") String size, @Query("api_key") String apiKey);
}

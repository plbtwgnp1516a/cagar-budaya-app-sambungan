package com.example.herlin.projectplbtw.editadmin.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by VAIO on 6/3/2016.
 */
public class EditAdminResponse {
    @SerializedName("Success")
    private Boolean Success;
    @SerializedName("Info")
    private String Info;

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

}

package com.example.herlin.projectplbtw;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.aboutus.AboutUsActivity;
import com.example.herlin.projectplbtw.admin.userinterface.AdminListActivity;
import com.example.herlin.projectplbtw.cagarbudaya.userinterface.CagarListActivity;
import com.example.herlin.projectplbtw.cagarbudaya.userinterface.PostedHerritageActivity;
import com.example.herlin.projectplbtw.challenge.userinterface.ChallengeActivity;
import com.example.herlin.projectplbtw.login.userinterface.LoginActivity;
import com.example.herlin.projectplbtw.reward.userinterface.RewardActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by VAIO on 5/23/2016.
 */
public class AdminActivity  extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";

    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";


    public Fragment about= new AboutUsActivity();
    public Fragment cagar = new CagarListActivity();
    public Fragment challenge = new ChallengeActivity();
    public Fragment reward = new RewardActivity();
    public Fragment postedCagar = new PostedHerritageActivity();
    public Fragment admin = new AdminListActivity();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
//        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        TextView nav_user = (TextView)hView.findViewById(R.id.textNama);
        TextView nav_email = (TextView)hView.findViewById(R.id.textEmail);
        nav_email.setText(sharedPreferences.getString(emailMarker,""));
        if(sharedPreferences.getString(roleMarker,"").equalsIgnoreCase("1"))
            nav_user.setText(sharedPreferences.getString(namaMarker,"")+" | Login as User");
        else
            nav_user.setText(sharedPreferences.getString(namaMarker," | login as Admin"));

        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, cagar)
                .commit();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      //  Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.herritage);
      //  fragment.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.about_us) {
            // Handle the camera action
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, about)
                    .commit();
        } else if (id == R.id.herritage) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, cagar)
                    .commit();

        } else if (id == R.id.reward) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, reward)
                    .commit();

        }else if (id == R.id.challenge) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, challenge)
                    .commit();

        }else if (id == R.id.logout) {
            clearSharedPreference();
            Intent i = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(i);
            this.finish();
        }else if (id == R.id.postedherritage) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, postedCagar)
                    .commit();

        }else if (id == R.id.admin) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, admin)
                    .commit();

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void clearSharedPreference() {
        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putBoolean(this.loginMarker, false);
        e.putString(namaMarker,"");
        e.putString(emailMarker,"");
        e.putString(poinMarker,"");
        e.putString(teleponMarker,"");
        e.putString(roleMarker,"");
        e.putString(idUserMarker,"");
        e.commit();
    }
}

package com.example.herlin.projectplbtw.detailreward.api;

import com.example.herlin.projectplbtw.detailreward.model.DetailRewardResponse;
import com.example.herlin.projectplbtw.detailreward.model.RedeemModel;
import com.example.herlin.projectplbtw.detailreward.model.RedeemResponse;
import com.example.herlin.projectplbtw.detailreward.model.UpdatePoinUserRedeem;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by VAIO on 5/31/2016.
 */
public interface ApiDetailReward {
    @DELETE("rewards/{id}")
    Call<DetailRewardResponse> deleteReward(@Path("id") String id);
    @POST("redeem")
    Call<RedeemResponse> postRedeem(@Body RedeemModel redeemModel);
    @PUT("user/redeempoin/{id_user}")
    Call<DetailRewardResponse> updatePoinUserAfterRedeem(@Path("id_user") String id_reward,
                                        @Body UpdatePoinUserRedeem updatePoinUserRedeem);
}

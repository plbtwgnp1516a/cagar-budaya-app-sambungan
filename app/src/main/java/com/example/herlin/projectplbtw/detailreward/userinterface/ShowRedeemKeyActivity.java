package com.example.herlin.projectplbtw.detailreward.userinterface;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.herlin.projectplbtw.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShowRedeemKeyActivity extends AppCompatActivity {
    @Bind(R.id.textRedeemKey)
    TextView textRedeemKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_redeem_key);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String redeem_key;
        Bundle b = getIntent().getExtras();
        redeem_key=b.getString("redeem_key");
        textRedeemKey.setText(redeem_key);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}

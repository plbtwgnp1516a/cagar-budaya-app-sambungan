package com.example.herlin.projectplbtw.admin.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.herlin.projectplbtw.AdminActivity;
import com.example.herlin.projectplbtw.OnLoadMoreListener;
import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.admin.api.ApiAdmin;
import com.example.herlin.projectplbtw.admin.model.AdminModel;
import com.example.herlin.projectplbtw.admin.model.AdminResponse;
import com.example.herlin.projectplbtw.admin.userinterface.AdminListActivity;
import com.example.herlin.projectplbtw.editadmin.userinterface.EditAdminActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by VAIO on 6/3/2016.
 */
public class RecyclerAdminAdapter extends RecyclerView.Adapter {
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";

    public static final String API_KEY = "asdfghjkl";


    private AdminListActivity adminActivity;
    public ApiAdmin apiAdmin = null;
    private final int TYPE_ADMIN = 1;
    private final int TYPE_LOAD_MORE = 2;


    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private int visibleThreshold = 1;   //determine when load more will run. "1" indicates that
    //load more will run when user scroll on the last article

    private Context context;
    private LayoutInflater inflater;
    private List<AdminModel> adminModelList;
    private CoordinatorLayout coordinatorLayout;
    private AdminViewHolder adminViewHolder;


    public void setAdminActivity(AdminListActivity adminActivity) {
        this.adminActivity = adminActivity;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setLoaded() {
        loading = false;
    }

    public RecyclerAdminAdapter
            (List<AdminModel> adminModelList, RecyclerView recyclerView) {
        apiAdmin = ServiceGenerator.createService(ApiAdmin.class);
        this.adminModelList = adminModelList;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (adminModelList.get(position) != null)
            return TYPE_ADMIN;
        else
            return TYPE_LOAD_MORE;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;
        View view;
        switch (viewType) {
            case TYPE_ADMIN:
                layoutId = R.layout.item_admin_list;
                view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
                return new AdminViewHolder(view);
            case TYPE_LOAD_MORE:
                layoutId = R.layout.progress_item;
                view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
                return new ProgressViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof AdminViewHolder)
            setAdminViewHolder(holder, position);
        else
            setProgressBarViewHolder(holder);


    }

    public void setProgressBarViewHolder(RecyclerView.ViewHolder holder) {
        ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
    }

    public void setCoordinatorLayout(CoordinatorLayout coordinatorLayout) {
        this.coordinatorLayout = coordinatorLayout;
    }


    public void setAdminViewHolder(RecyclerView.ViewHolder holder, final int position) {

        adminViewHolder = (AdminViewHolder) holder;
        adminViewHolder.adminModel = adminModelList.get(position);
        sharedPreferences = holder.itemView.getContext().getSharedPreferences(sharedPreferenceName, 0);
        adminViewHolder.textNamaAdmin.setText(adminViewHolder
                .adminModel.getNama() + "");
        adminViewHolder.textEmail.setText(adminViewHolder
                .adminModel.getEmail() + "");
        adminViewHolder.textTelepon.setText(adminViewHolder.adminModel.getTelepon());
        adminViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionMenu(adminViewHolder, v, position);
            }
        });
    }

    private void showOptionMenu(final AdminViewHolder adminViewHolder, final View view, final int position) {
        final String[] items;
        final Integer[] icons;
        items = new String[]{"Edit Admin", "Delete Admin"};
        icons = new Integer[]{R.drawable.ic_mode_edit_black_24dp, R.drawable.ic_clear_black_24dp};

        ListAdapter listAdapter = new ArrayAdapterWithIcon(view.getContext(), items, icons);
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setAdapter(listAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int item) {
                if (items[item].equals("Edit Admin")) {
                    showEditAdmin(adminViewHolder, view, position);
                } else if (items[item].equals("Delete Admin")) {
                    if (sharedPreferences.getString(idUserMarker, "").equalsIgnoreCase(adminModelList.get(0).getId_user()))
                        Snackbar.make(coordinatorLayout, "Tidak dapat menghapus diri sendiri",
                                Snackbar.LENGTH_LONG).show();
                    else
                        showDeleteAdmin(adminViewHolder, view,position);
                }
            }
        });
        builder.show();
    }

    private void showEditAdmin(final AdminViewHolder adminViewHolder, final View view, int position) {
        Intent intent = new Intent(view.getContext(), EditAdminActivity.class);

        Bundle b = new Bundle();
        b.putString("id_user", adminModelList.get(position).getId_user());
        b.putString("email", adminModelList.get(position).getEmail());
        b.putString("nama", adminModelList.get(position).getNama());
        b.putString("telepon", adminModelList.get(position).getTelepon());
        intent.putExtras(b);
        adminActivity.startActivity(intent);
    }

    private void showDeleteAdmin(final AdminViewHolder adminViewHolder, final View view, final int position) {
        final android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(adminActivity.getContext());
        alertDialog.setMessage(Html.fromHtml("<strong>Apakah anda yakin ingin menghapus admin" + adminModelList.get(position).getNama() + "?</strong>"));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
                final ProgressDialog dialog = new ProgressDialog(view.getContext());
                dialog.setMessage("Please wait...");
                dialog.show();
                Call<AdminResponse> call = null;
                call = apiAdmin.deleteAdmin(adminModelList.get(position).getId_user(), API_KEY);
                call.enqueue(new Callback<AdminResponse>() {
                    @Override
                    public void onResponse(Response<AdminResponse> adminResponse, Retrofit retrofit) {
                        //  Toast.makeText(AdminListActivity.this, adminResponse.code()+"", Toast.LENGTH_SHORT).show();

                        if (2 == adminResponse.code() / 100) {
                            dialog.dismiss();
                            Snackbar.make(coordinatorLayout, "Admin berhasil terhapus",
                                    Snackbar.LENGTH_LONG).show();
                            //      Toast.makeText(DetailCagarActivity.this, "Cagar Budaya Terhapus", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.dismiss();
                            Snackbar.make(coordinatorLayout, "Gagal menghapus admin",
                                    Snackbar.LENGTH_LONG).show();

                            //      Toast.makeText(DetailCagarActivity.this, "Error dalam menghapus Cagar Budaya ", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Snackbar.make(coordinatorLayout, "Tejadi kesalahan jaringan..",
                                Snackbar.LENGTH_LONG).show();

                        dialog.dismiss();
//                        Toast.makeText(DetailCagarActivity.this, "Gagal menghapus cagar budaya", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
            }
        });
        alertDialog.show();

    }

    @Override
    public int getItemCount() {
        return (adminModelList.size());
    }


    public static class AdminViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textNamaAdmin)
        TextView textNamaAdmin;
        @Bind(R.id.textEmail)
        TextView textEmail;
        @Bind(R.id.textTelepon)
        TextView textTelepon;

        AdminModel adminModel;

        public AdminViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.progressBar1)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, itemView);
        }
    }
}

package com.example.herlin.projectplbtw.login.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by VAIO on 5/22/2016.
 */
public class LoginObjectResponse {

    @SerializedName("id_user")
    private String id_user;

    @SerializedName("nama")
    private String nama;

    @SerializedName("email")
    private String email;

    @SerializedName("telepon")
    private String telepon;

    @SerializedName("poin")
    private String poin;

    @SerializedName("id_role")
    private String id_role;
   // private String role;
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getPoin() {
        return poin;
    }

    public void setPoin(String poin) {
        this.poin = poin;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_role() {
        return id_role;
    }

    public void setId_role(String id_role) {
        this.id_role = id_role;
    }
}

package com.example.herlin.projectplbtw.editreward.model;

/**
 * Created by VAIO on 6/1/2016.
 */
public class EditRewardModel {
    private String nama_reward;
    private String deskripsi;
    private String minimal_poin;

    public String getNama_reward() {
        return nama_reward;
    }

    public void setNama_reward(String nama_reward) {
        this.nama_reward = nama_reward;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getMinimal_poin() {
        return minimal_poin;
    }

    public void setMinimal_poin(String minimal_poin) {
        this.minimal_poin = minimal_poin;
    }
}

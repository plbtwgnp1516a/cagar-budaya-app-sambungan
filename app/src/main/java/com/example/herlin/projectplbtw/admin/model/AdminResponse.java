package com.example.herlin.projectplbtw.admin.model;

/**
 * Created by VAIO on 6/1/2016.
 */
public class AdminResponse {
    private Boolean Success;
    private String Info;

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

}

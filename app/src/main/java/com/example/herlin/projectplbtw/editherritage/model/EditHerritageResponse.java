package com.example.herlin.projectplbtw.editherritage.model;

public class EditHerritageResponse {
    private Boolean Success;
    private String Info;

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

}

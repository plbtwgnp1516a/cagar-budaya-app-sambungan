package com.example.herlin.projectplbtw.addadmin.userinterface;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.addadmin.api.ApiAddAdmin;
import com.example.herlin.projectplbtw.addadmin.model.AddAdminModel;
import com.example.herlin.projectplbtw.addadmin.model.AddAdminResponse;
import com.example.herlin.projectplbtw.addchallenge.api.ApiAddChallenge;
import com.example.herlin.projectplbtw.editadmin.userinterface.EditAdminActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AddAdminActivity extends AppCompatActivity {

    public static final String API_KEY = "asdfghjkl";
    private ApiAddAdmin apiAddAdmin = null;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @Bind(R.id.textNama)
    EditText textNamaAdmin;
    @Bind(R.id.textEmail)
    EditText textEmail;
    @Bind(R.id.textTelepon)
    EditText textTelepon;
    @Bind(R.id.textPassword)
    EditText textPassword;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnAddAdmin)
    Button btnAddAdmin;

    @OnClick(R.id.btnCancel)
    void setOnClickBtnCancel(View view) {
        finish();
    }

    @OnClick(R.id.btnAddAdmin)
    void setOnClickBtnAddAdmin(View view) {
        if (textNamaAdmin.getText().toString().isEmpty() || textEmail.getText().toString().isEmpty() || textTelepon.getText().toString().isEmpty()|| textPassword.getText().toString().isEmpty()) {
            Snackbar.make(view, "Tolong isi data yang kosong!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {
            final ProgressDialog dialog = new ProgressDialog(AddAdminActivity.this);
            dialog.setMessage("Mohon tunggu...");
            dialog.setCancelable(false);
            dialog.show();
            Call<AddAdminResponse> call = null;

            AddAdminModel addAdminModel = new AddAdminModel();
            addAdminModel.setApi_key(API_KEY);
            addAdminModel.setNama(textNamaAdmin.getText().toString());
            addAdminModel.setTelepon(textTelepon.getText().toString());
            addAdminModel.setEmail(textEmail.getText().toString());
            addAdminModel.setNama_role("admin");
            addAdminModel.setPoin("0");
            addAdminModel.setPassword(textPassword.getText().toString());
            call = apiAddAdmin.postAdmin(addAdminModel);

            call.enqueue(new Callback<AddAdminResponse>() {
                @Override
                public void onResponse(Response<AddAdminResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
           //         Toast.makeText(AddAdminActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                           finish();
                        else
                            Snackbar.make(coordinatorLayout, "Gagal Menambahkan Admin", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                    } else {
                        Snackbar.make(coordinatorLayout, "Gagal Menambahkan Admin", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout, "Kesalahan Jaringan..", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    // textNamaCagar.setText(t.getMessage()+"");
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_admin);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiAddAdmin = ServiceGenerator.createService(ApiAddAdmin.class);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

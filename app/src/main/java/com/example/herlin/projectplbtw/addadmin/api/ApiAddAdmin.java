package com.example.herlin.projectplbtw.addadmin.api;

import com.example.herlin.projectplbtw.addadmin.model.AddAdminModel;
import com.example.herlin.projectplbtw.addadmin.model.AddAdminResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by VAIO on 6/3/2016.
 */
public interface ApiAddAdmin {
    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST("user/signup")
    Call<AddAdminResponse> postAdmin(@Body AddAdminModel addAdminModel);
}

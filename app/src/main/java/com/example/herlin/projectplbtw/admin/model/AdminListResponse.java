package com.example.herlin.projectplbtw.admin.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by VAIO on 6/3/2016.
 */
public class AdminListResponse {
    @SerializedName("content")
    private ArrayList<AdminModel> listAdmin = new ArrayList<>();
    @SerializedName("totalPages")
    private int totalPages ;

    public ArrayList<AdminModel> getListAdmin() {
        return listAdmin;
    }

    public void setListAdmin(ArrayList<AdminModel> listAdmin) {
        this.listAdmin = listAdmin;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}

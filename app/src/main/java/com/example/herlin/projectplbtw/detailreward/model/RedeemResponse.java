package com.example.herlin.projectplbtw.detailreward.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by VAIO on 6/3/2016.
 */
public class RedeemResponse {
    @SerializedName("content")
    private ArrayList<RedeemObjectResponse> content=new ArrayList<RedeemObjectResponse>();

    @SerializedName("Info")
    private String Info;

    public ArrayList<RedeemObjectResponse> getContent() {
        return content;
    }

    public void setContent(ArrayList<RedeemObjectResponse> content) {
        this.content = content;
    }

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }
}

package com.example.herlin.projectplbtw;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.herlin.projectplbtw.addherritage.api.ApiAddHerritage;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageResponse;
import com.example.herlin.projectplbtw.addherritage.model.AddPointModel;
import com.example.herlin.projectplbtw.addherritage.userinterface.AddHerritageActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

public class ShareTwitActivity extends AppCompatActivity {
    private ApiAddHerritage apiAddHerritage = null;
    public static final String API_KEY="asdfghjkl";

    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";

    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";

    private static Twitter twitter;
    private static RequestToken requestToken;

    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;

    private String nama_cagar;
    private String nama_jenis;
    private String alamat;

    private static SharedPreferences mSharedPreferences;

    private ProgressDialog pDialog;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.btnSendTweet)
    Button btnSendTweet;
    @Bind(R.id.txtTweet)
    EditText txtTweet;
    @OnClick(R.id.btnSendTweet)
    void sendTweet(View view)
    {
        final String status = txtTweet.getText().toString();

        if (status.trim().length() > 0) {
            new updateTwitterStatus().execute(status);
        } else {
            Toast.makeText(this, "Message is empty!!", Toast.LENGTH_SHORT).show();
        }

    }

    /* Reading twitter essential configuration parameters from strings.xml */
    private void initTwitterConfigs() {
        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        callbackUrl = getString(R.string.twitter_callback);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_twit);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiAddHerritage = ServiceGenerator.createService(ApiAddHerritage.class);
        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        mSharedPreferences = getSharedPreferences(PREF_NAME, 0);
        initTwitterConfigs();
        Bundle b = getIntent().getExtras();
        nama_cagar=b.getString("nama_cagar");
        nama_jenis=b.getString("nama_jenis");
        alamat =b.getString("alamat");
        txtTweet.setText("Ayo berkunjung ke "+alamat+"." +
                " Disana ada sebuah "+nama_jenis+" bernama "+nama_cagar+".\n\n" +
                "#DownloadSekarang\n" +
                "#CabuD");
   //     txtTweet.setEnabled(false);
    }

    class updateTwitterStatus extends AsyncTask<String,String, Void> {

        public updateTwitterStatus()
        {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(ShareTwitActivity.this);
            pDialog.setMessage("Posting to twitter...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
//            Toast.makeText(getApplicationContext(),bis.toString(),Toast.LENGTH_LONG).show();
        }

        protected Void doInBackground(String... args) {

            String status = args[0];
            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(consumerKey);
                builder.setOAuthConsumerSecret(consumerSecret);

                // Access Token
                String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
                // Access Token Secret
                String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);


                // Update status
                StatusUpdate statusUpdate = new StatusUpdate(status);

                //  add image to status

                //     InputStream is = this.bis;//getResources().openRawResource(R.drawable.ic_launcher);
                //     statusUpdate.setMedia("test.jpg", is);
               // statusUpdate.setMedia(bis);
                twitter4j.Status response = twitter.updateStatus(statusUpdate);

                //upload profile image, but in do in background you must do a single task
                //		twitter.updateProfileImage(is);

                Log.d("Status", response.getText());


            } catch (TwitterException e) {
                Log.d("Failed to post!", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
			/* Dismiss the progress dialog after sharing */
            pDialog.dismiss();
            Toast.makeText(ShareTwitActivity.this, "Posted to Twitter!", Toast.LENGTH_SHORT).show();
            // Clearing EditText field
            txtTweet.setText("");
           addUserPoint();
        }
    }
    void addUserPoint()
    {
        final ProgressDialog dialog = new ProgressDialog(ShareTwitActivity.this);
        dialog.setMessage("Updating user point");
        dialog.setCancelable(false);
        dialog.show();
        Call<AddHerritageResponse> call = null;
        AddPointModel addPointModel = new AddPointModel();
        addPointModel.setApi_key(API_KEY);
        addPointModel.setPoin("10");
        call = apiAddHerritage.addPoint(sharedPreferences.getString(idUserMarker,""),addPointModel);

        call.enqueue(new Callback<AddHerritageResponse>() {
            @Override
            public void onResponse(Response<AddHerritageResponse> response, Retrofit retrofit) {
                dialog.dismiss();
                //         Toast.makeText(AddHerritageActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                if (2 == response.code() / 100) {
                    // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                    if (response.body().getSuccess() == true)
                        finish();
                    else
                        Snackbar.make(coordinatorLayout, "Gagal menambahkan poin", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    //finish();
                } else {
                    Snackbar.make(coordinatorLayout, "Gagal menambahkan poin", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                //     textNamaCagar.setText(t.getMessage()+"");

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

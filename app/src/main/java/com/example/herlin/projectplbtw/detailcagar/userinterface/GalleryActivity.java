package com.example.herlin.projectplbtw.detailcagar.userinterface;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.cagarbudaya.model.CagarMultimediaModel;
import com.example.herlin.projectplbtw.detailcagar.adapter.RecyclerGalleryAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GalleryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this, rootview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getApplicationContext()));


        progressBar.setVisibility(View.VISIBLE);

        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        String role = sharedPreferences.getString(roleMarker, "0");

        //getChallengeList(true);

    }
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";

    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";


    View rootview;

    public static final String API_KEY = "asdfghjkl";
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    //   @Bind(R.id.contentChallengeList)
    //   LinearLayout contentChallengeList;

    @Bind(R.id.listChallenge)
    RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<CagarMultimediaModel> challengeModelList;
    private RecyclerGalleryAdapter adapter;

/*
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        rootview = inflater.inflate(R.layout.activity_challenge, container, false);
        //       getActivity().setTitle("Challenge");
        ButterKnife.bind(this, rootview);

    }


    @Override
    public void onStart() {
        super.onStart();
        getChallengeList(true);
        Snackbar.make(coordinatorLayout, "Sedang memperbarui data tantangan..", Snackbar.LENGTH_LONG).show();
    }

    private void getChallengeList(boolean pBar) {
        if (pBar)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
        Call<ChallengeResponse> call = null;
        call = apiChallenge.getChallengeList("1", "1000", API_KEY);

//        call = apiReward.getHerritageList();
        call.enqueue(new Callback<ChallengeResponse>() {
            @Override
            public void onResponse(Response<ChallengeResponse> response,
                                   Retrofit retrofit) {

                if (2 == response.code() / 100 && response.isSuccess()) {
                    showChallengeList(response);
                } else {
                    showErrorMessage();
                }
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                //        contentChallengeList.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan..",
                        Snackbar.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                //         contentChallengeList.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showErrorMessage() {
        Snackbar.make(coordinatorLayout, "Gagal memperbarui data tantangan", Snackbar.LENGTH_LONG).show();
    }

    private void showChallengeList
            (Response<ChallengeResponse> response) {

        final ChallengeResponse challengeResponse = response.body();
        challengeModelList = challengeResponse.getListChallenge();
        if (challengeModelList != null) {
            adapter = new RecyclerChallengeAdapter(challengeModelList, recyclerView);
            adapter.setCoordinatorLayout(coordinatorLayout);
            if (getActivity() != null) {
                adapter.setContext(getActivity().getApplicationContext());
                adapter.setInflater(getActivity().getLayoutInflater());
                adapter.setChallengeActivity(this);
                recyclerView.setAdapter(adapter);
            }
        }

    }
*/
}

package com.example.herlin.projectplbtw.addherritage.userinterface;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.herlin.projectplbtw.GPSTracker;
import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.addherritage.api.ApiAddHerritage;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageModel;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageResponse;
import com.example.herlin.projectplbtw.addherritage.model.AddPointModel;
import com.example.herlin.projectplbtw.addherritage.model.CagarMultimedia;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AddHerritageActivity extends AppCompatActivity {
    private String thumbnailCode;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddHerritageActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
      //  alamat=destination.getPath();
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        roundedImage = new RoundImage(thumbnail);
//        ivImage.setImageDrawable(roundedImage);
          imageThumbnail.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);
        //alamat = selectedImagePath;
        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

   //     roundedImage = new RoundImage(bm);
   //     ivImage.setImageDrawable(roundedImage);
             imageThumbnail.setImageBitmap(bm);
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        return imgString;
    }

    GPSTracker gps;
    public void getMyLocationAddress() {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            //Place your latitude and longitude
            List<Address> addresses = geocoder.getFromLocation(gps.getLatitude(), gps.getLongitude(), 1);

            if (addresses != null) {

                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();

                for (int i = 0; i < fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append("\n");
                }
                Toast.makeText(AddHerritageActivity.this, "I am at"+strAddress , Toast.LENGTH_SHORT).show();
                textAlamat.setText(""+strAddress);
                latitude=gps.getLatitude()+"";
                longitude=gps.getLongitude()+"";
            } else {
                Toast.makeText(AddHerritageActivity.this, "No location found!", Toast.LENGTH_SHORT).show();
                textAlamat.setText("No location found!!");
            }
        }
        catch (java.io.IOException e)
        {

        }
    }

    private String latitude;
    private String longitude;

    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";

    private ApiAddHerritage apiAddHerritage = null;
    public static final String API_KEY="asdfghjkl";
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.textNamaCagar)
    EditText textNamaCagar;
    @Bind(R.id.textDeskripsi)
    EditText textDeskripsi;
    @Bind(R.id.textTahun)
    EditText textTahun;
    @Bind(R.id.spinJenis)
    Spinner spinJenis;
    @Bind(R.id.textAlamat)
    EditText textAlamat;
    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnAddCagar)
    Button btnAddCagar;
    @Bind(R.id.btnSelectImage)
    Button btnSelectImage;

    @OnClick(R.id.btnSelectImage)
    void setOnClickBtnSelectImage(View view)
    {
        selectImage();
    }

    @OnClick(R.id.btnCancel)
    void backToMenu(View view) {
        this.finish();
    }

    @OnClick(R.id.btnAddCagar)
    void setOnClickBtnAddCagar(View view) {
        if (textAlamat.getText().toString().isEmpty() || textDeskripsi.getText().toString().isEmpty() || textNamaCagar.getText().toString().isEmpty() || textTahun.getText().toString().isEmpty()) {
            Snackbar.make(view, "Tolong isi data yang kosong!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        else
        {
            final ProgressDialog dialog = new ProgressDialog(AddHerritageActivity.this);
            dialog.setMessage("Mohon tunggu...");
            dialog.setCancelable(false);
            dialog.show();
            Call<AddHerritageResponse> call = null;

            AddHerritageModel addHerritageModel = new AddHerritageModel();
            addHerritageModel.setApi_key(API_KEY);
            addHerritageModel.setAlamat(textAlamat.getText().toString());
            addHerritageModel.setDeskripsi(textDeskripsi.getText().toString());
            addHerritageModel.setId_user(sharedPreferences.getString(idUserMarker, ""));
            addHerritageModel.setLatitude(latitude); //harus dr maps
            addHerritageModel.setLongitude(longitude);  //harus dr maps
            addHerritageModel.setNama_cagar(textNamaCagar.getText().toString());
            addHerritageModel.setStatus("pending");
            addHerritageModel.setTahun_peninggalan(textTahun.getText().toString());
            addHerritageModel.setNama_jenis(spinJenis.getSelectedItem().toString());
            CagarMultimedia cagarMultimedia=new CagarMultimedia();
            cagarMultimedia.setId_user(sharedPreferences.getString(idUserMarker,""));
            cagarMultimedia.setNama_multimedia("gambar1");
            cagarMultimedia.setTipe("Gambar");

            Bitmap bitmap = ((BitmapDrawable) imageThumbnail.getDrawable()).getBitmap();
            thumbnailCode = getEncoded64ImageStringFromBitmap(bitmap);
            cagarMultimedia.setImage_base64(thumbnailCode);
            ArrayList<CagarMultimedia> mulmed=new ArrayList<CagarMultimedia>();
            mulmed.add(cagarMultimedia);

            addHerritageModel.setMultimedia(mulmed);

            call = apiAddHerritage.postHerritage(addHerritageModel);

            call.enqueue(new Callback<AddHerritageResponse>() {
                @Override
                public void onResponse(Response<AddHerritageResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
               //     Toast.makeText(AddHerritageActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                       // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                            addUserPoint();
                        else
                            Snackbar.make(coordinatorLayout, "Gagal menambahkan cagar budaya", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                    } else {
                        Snackbar.make(coordinatorLayout, "Gagal menambahkan cagar budaya", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout,t.getMessage()+"Terjadi Kesalahan Jaringan..", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
               //     textNamaCagar.setText(t.getMessage()+"");

                }
            });
        }
    }
    void addUserPoint()
    {
        final ProgressDialog dialog = new ProgressDialog(AddHerritageActivity.this);
        dialog.setMessage("Updating user point");
        dialog.setCancelable(false);
        dialog.show();
        Call<AddHerritageResponse> call = null;
        AddPointModel addPointModel = new AddPointModel();
        addPointModel.setApi_key(API_KEY);
        addPointModel.setPoin("100");
        call = apiAddHerritage.addPoint(sharedPreferences.getString(idUserMarker,""),addPointModel);

        call.enqueue(new Callback<AddHerritageResponse>() {
            @Override
            public void onResponse(Response<AddHerritageResponse> response, Retrofit retrofit) {
                dialog.dismiss();
       //         Toast.makeText(AddHerritageActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                if (2 == response.code() / 100) {
                    // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                    if (response.body().getSuccess() == true)
                        moveToMenu();
                    else
                        Snackbar.make(coordinatorLayout, "Gagal menambahkan poin", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                } else {
                    Snackbar.make(coordinatorLayout, "Gagal menambahkan poin", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Snackbar.make(coordinatorLayout, "Terjadi kesalahan jaringan", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                //     textNamaCagar.setText(t.getMessage()+"");

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_herritage);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
   //     setTitle("Add Herritage");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiAddHerritage = ServiceGenerator.createService(ApiAddHerritage.class);
        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        // TODO Auto-generated method stub
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        latitude="";
        longitude="";
        gps = new GPSTracker(AddHerritageActivity.this);

        // check if GPS enabled
        if(gps.canGetLocation()){

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            // \n is for new line
            Toast.makeText(getApplicationContext(), "Mencari longitude dan latitude..", Toast.LENGTH_LONG).show();
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
        getMyLocationAddress();
    }

    private void moveToMenu()
    {
        Toast.makeText(AddHerritageActivity.this, "Berhasil menambahkan cagar budaya!", Toast.LENGTH_SHORT).show();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

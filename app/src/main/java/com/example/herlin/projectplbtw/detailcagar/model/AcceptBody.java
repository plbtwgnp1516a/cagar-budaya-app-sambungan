package com.example.herlin.projectplbtw.detailcagar.model;

/**
 * Created by VAIO on 6/3/2016.
 */
public class AcceptBody {
    private String api_key;
    private String status;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.example.herlin.projectplbtw.detailreward.model;

/**
 * Created by VAIO on 6/3/2016.
 */
public class RedeemModel {
    private String id_reward;
    private String id_user;

    public String getId_reward() {
        return id_reward;
    }

    public void setId_reward(String id_reward) {
        this.id_reward = id_reward;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }
}

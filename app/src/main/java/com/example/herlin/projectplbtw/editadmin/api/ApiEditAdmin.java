package com.example.herlin.projectplbtw.editadmin.api;

import com.example.herlin.projectplbtw.editadmin.model.EditAdminModel;
import com.example.herlin.projectplbtw.editadmin.model.EditAdminResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by VAIO on 6/3/2016.
 */
public interface ApiEditAdmin {
    @PUT("challenge/{id_user}")
    Call<EditAdminResponse> editAdmin(@Path("id_user") String id_user,
                                      @Body EditAdminModel editAdminModel);
}

package com.example.herlin.projectplbtw.reward.model;

/**
 * Created by VAIO on 5/16/2016.
 */
public class RewardModel {
    private String id_reward;
    private String nama_reward;
    private String deskripsi;
    private String minimal_poin;


    public String getId_reward() {
        return id_reward;
    }

    public void setId_reward(String id_reward) {
        this.id_reward = id_reward;
    }

    public String getNama_reward() {
        return nama_reward;
    }

    public void setNama_reward(String nama_reward) {
        this.nama_reward = nama_reward;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getMinimal_poin() {
        return minimal_poin;
    }

    public void setMinimal_poin(String minimal_poin) {
        this.minimal_poin = minimal_poin;
    }
}

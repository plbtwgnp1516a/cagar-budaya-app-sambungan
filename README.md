**# README OF CABUD #

This README would normally document whatever steps are necessary to get your application up and running.
### What is this repository for? ###
Repositori ini digunakan untuk menyimpan aplikasi Cabud. Aplikasi Cabud merupakan aplikasi yang menggunakan private API untuk menampilkan Cagar Budaya di Indonesia. Aplikasi ini juga dilengkapi dengan fitur gamification yang memberikan peluang bagi anda untuk dapat memperoleh reward dengan cara mengikuti challenge yang disediakan.

### How do I get set up? ###
Clone aplikasi ini. Kemudian buka dengan menggunakan Android Studio. Setelah terbuka untuk pertama kali, biasanya akan dilakukan proses download, karena aplikasi ini menggunakan open source library dalam pembuatannya. Jadi pastikan anda terkoneksi dengan Internet.

### Who do I talk to? ###
Penanggung jawab repositori ini adalah Toni Indrawan | 130707552